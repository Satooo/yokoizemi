<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ログイン画面</title>

<!-- BootstrapのCSS読み込み -->
<link href="${f:url('/css/bootstrap.css')}" rel="stylesheet">
<link href="${f:url('/css/login.css')}" rel="stylesheet">
<!-- jQuery読み込み -->
<script src="${f:url('/js/jquery-3.1.1.min.js')}"></script>
<!-- BootstrapのJS読み込み -->
<script src="${f:url('/js/bootstrap.js')}"></script>
</head>

<body>

	<div class="container">
		<div class="wrapper">
			<form class="form-signin" method="POST">
				<h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
				<hr class="colorgraph">
				<br> <input type="text" class="form-control" name="userId"
					placeholder="userId" /> <input type="password" class="form-control"
					name="password" placeholder="password" />

				<button class="btn btn-lg btn-info btn-block" name="Submit"
					value="Login" type="Submit">Login</button>
			</form>

		</div>
	</div>

</body>
</html>