<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>外部リンクトップページ</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<s:form method="POST">
					<h3>外部リンクトップページ</h3>
					<h2>外部リンク</h2>
					<table id="DVDinfo" class="basetable">
						<thead>
							<tr>
								<th>HCS</th>
								<td><a href="http://www.hcs.ac.jp/">www.hcs.ac.jp</a></td>
							</tr>
							<tr>
								<th>リクナビ</th>
								<td><a href="http://www.rikunabi.com/">www.rikunabi.com</a></td>
							</tr>
							<tr>
								<th>google</th>
								<td><a href="https://www.google.co.jp/">https://www.google.co.jp</a></td>
							</tr>
							<tr>
								<th>蕎麦打ちの科学</th>
								<td><a href="http://kumatako.o.oo7.jp/">http://kumatako.o.oo7.jp/</a>
								</td>
							</tr>

						</thead>
					</table>
					<html:errors />
					<br>
					<br>
					<div class="center">
						<s:submit value="Topへ" property="menu3" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>