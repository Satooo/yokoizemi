<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>事務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>商品掲載</h3>
				<table id="DVDinfo" class="table table-striped table-bordered">
					<thead>

						<tr>
							<td>商品名</td>
							<td>価格</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>ノート</td>
							<td>100円</td>
						</tr>
						<tr>
							<td>テンプレート</td>
							<td>300円</td>
						</tr>
						<tr>
							<td>履歴書</td>
							<td>100円</td>
						</tr>
					</tbody>

				</table>
				<html:errors />
				<br> <br>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>