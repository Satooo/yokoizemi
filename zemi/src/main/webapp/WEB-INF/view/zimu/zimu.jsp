<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>事務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>事務部</h3>
				<table id="DVDinfo" class="table table-bordered">
					<s:form method="POST">
						<tr>
							<th><s:submit value="事務バイト情報" property="jobInfo"
									styleClass="btn btn-info btn-block" /></th>
							<td>バイト情報を表示します。</td>
						</tr>
					</s:form>

					<s:form method="POST">
						<tr>
							<th><s:submit value="事務商品情報" property="productInfo"
									styleClass="btn btn-info btn-block" /></th>
							<td>事務の商品情報を表示します。</td>
						</tr>
					</s:form>

					<s:form method="POST">
						<tr>
							<th><s:submit value="奨学金情報" property="shogakuInfo"
									styleClass="btn btn-info btn-block" /></th>
							<td>奨学金情報を表示します。</td>
						</tr>
					</s:form>
				</table>
				<html:errors />
				<br> <br>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>