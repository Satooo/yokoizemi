<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>事務部</title>
</head>
<body>
    <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
        <tiles:put name="title" value="Hello SAStruts!" />
        <tiles:put name="content" type="string">
            <article>
                <h3>事務バイト情報</h3>
                <table id="DVDinfo" class="basetable">
                    <thead>

                    <tr>
                    <td>ピザ屋募集
                    <br>条件：高卒以上
                    <br>締め切り：12月22日
                    </td>
                    
                    <tr>
                    <td>アークス募集
                    <br>条件：高卒以上
                    <br>締め切り：12月30日
                    </td>
                    
                    <tr>
                    <td>セブンイレブン募集
                    <br>条件：高卒以上
                    <br>締め切り：12月30日
                    </td>
                    
                    <tr>
                    <td>マックスバリュー募集
                    <br>条件：高卒以上
                    <br>締め切り：12月30日
                    </td>
                    
                     <tr>
                    <td>パン屋募集
                    <br>条件：高卒以上
                    <br>締め切り：4月1日
                    </td>
                    
                     <tr>
                    <td>蕎麦屋募集
                    <br>条件：高卒以上
                    <br>締め切り：1月30日
                    </td>
                    
                    <tr>
                    <td>SCC募集
                    <br>条件:応用情報取得者
                    <br>締め切り：1月30日
                    </td>
                    

                    </thead>
                </table>
                <html:errors />
                <br> <br>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>