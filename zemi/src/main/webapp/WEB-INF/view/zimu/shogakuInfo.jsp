<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">

<title>事務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>奨学金情報</h3>
				<html:errors />
				<table class="table" id="syain_table">
					<thead>
						<tr>
							<th>掲載日</th>
							<th>ジャンル</th>
							<th>お知らせ<br>タイトル
							</th>
							<th>お知らせ内容</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="notification" varStatus="c"
							items="${notificationList}">
							<c:if test="${f:h(notification.notification_genre_code)==03}">

								<tr>
									<td>${f:h(notification.published_day)}</td>
									<td>${f:h(notification.notification_genre_codeLabel)}</td>
									<td><s:link
											href="/notification/notification_detail?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
                                            &published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">${f:h(notification.notification_title)}</s:link></td>
									<td>${f:h(notification.notification_content)}</td>
								</tr>

							</c:if>
						</c:forEach>
					</tbody>
				</table>

				<html:errors />
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>