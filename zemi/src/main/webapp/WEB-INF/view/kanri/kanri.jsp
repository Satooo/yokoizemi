<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>管理部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>管理トップページ</h3>
				<h2>管理</h2>
				<table id="DVDinfo" class="basetable">
					<thead>
						<s:form method="POST">
							<tr>
								<th><s:submit value="お知らせ情報" property="notification_admin"
										styleClass="btn3" /></th>
								<td>お知らせ情報を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="バイト情報" property="baito" style="WIDTH: 110px; HEIGHT: 30px"/></th>
								<td>バイト情報を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="掲示板" property="keijiban" style="WIDTH: 110px; HEIGHT: 30px"/></th>
								<td>掲示板情報を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="要望" property="yobo" style="WIDTH: 110px; HEIGHT: 30px"/></th>
								<td>要望を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="講義予定" property="kogiyotei" style="WIDTH: 110px; HEIGHT: 30px"/></th>
								<td>講義予定を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="広報" property="koho" style="WIDTH: 110px; HEIGHT: 30px"/></th>
								<td>広報情報を表示します。</td>
							</tr>
						</s:form>
					</thead>
				</table>
				<html:errors />
				<br> <br>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>