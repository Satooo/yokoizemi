<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="${f:url('/css/common.css')}" />
<link rel="stylesheet" href="${f:url('/css/jquery.dataTables.min.css')}" />
<script src="${f:url('/js/jquery-1.9.1.min.js')}"></script>
<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
<script src="${f:url('/js/dataTable_sub.js')}"></script>
</head>
<body>
    <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
        <tiles:put name="title" value="Hello SAStruts!" />
        <tiles:put name="content" type="string">
            <article>
                <h3>バイト一覧</h3>
                <html:errors />
                <s:form method="POST">
                    <table class="dataTable" id="syain_table">
                        <thead>
                            <tr>
                                <th>作成日時</th>
                                <th>タイトル</th>
                                <th>公開</th>
                                <th>削除</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="part" varStatus="c"
                                items="${partList}">
                                <tr>
                                    <td>${f:h(part.published_start)}</td>
                                    <td>${f:h(part.file_name)}</td>
                                    <td>${f:h(part.published_flg)}</td>
                                    <td><button class="btn">更新</button></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </s:form>
            </article>
        </tiles:put>
    </tiles:insert>
</body>
</html>