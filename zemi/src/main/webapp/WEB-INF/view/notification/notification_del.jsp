<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
					<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>お知らせ削除</h3>
				<html:errors />
				<s:form method="POST">
					<html:hidden property="notification_code"
						value="${f:h(notification_code)}" />
					<html:hidden property="notification_genre_code"
						value="${f:h(notification_genre_code)}" />
					<html:hidden property="notification_title"
						value="${f:h(notification_title)}" />
					<html:hidden property="notification_content"
						value="${f:h(notification_content)}" />
					<html:hidden property="published_day" value="${f:h(published_day)}" />
					<html:hidden property="published_flg" value="${f:h(published_flg)}" />
					<table class="basetable" id="syain_table">
						<tr>
							<th>タイトル</th>
							<td>${f:h(notification_title)}</td>
						</tr>
						<tr>
							<th>内容</th>
							<td>${f:h(notification_content)}</td>
						</tr>
					</table>
					<br>
					<div Align="center">
						<s:submit property="notification_del_fin" value="削除"
							styleClass="button" />
						<s:submit property="notification_admin" value="戻る"
							styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>