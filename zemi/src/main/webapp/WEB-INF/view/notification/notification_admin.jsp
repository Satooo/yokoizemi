<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
	$(function() {
		var $setElm = $('td');
		var cutFigure = '10'; // カットする文字数
		var afterTxt = ' …'; // 文字カット後に表示するテキスト

		$setElm.each(function() {
			var textLength = $(this).text().length;
			var textTrim = $(this).text().substr(0, (cutFigure))

			if (cutFigure < textLength) {
				$(this).html(textTrim + afterTxt).css({
					visibility : 'visible'
				});
			} else if (cutFigure >= textLength) {
				$(this).css({
					visibility : 'visible'
				});
			}
		});
	});
</script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>お知らせ管理者用</h3>
				<html:errors />
				<s:form method="POST">
					<div Align="right">
						<s:link href="notification_ins">新規作成</s:link>
					</div>
					<table class="dataTable" id="syain_table">
						<thead>
							<tr>
								<th>掲載日</th>
								<th>ジャンル</th>
								<th>お知らせタイトル</th>
								<th>編集</th>
								<th>削除</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="notification" varStatus="c"
								items="${notificationList}">
								<tr>
									<td>${f:h(notification.published_day)}</td>
									<td>${f:h(notification.notification_genre_codeLabel)}</td>
									<td>${f:h(notification.notification_title)}</td>
									<html:hidden property="notification_code"
										value="${f:h(notification.notification_code)}" />
									<html:hidden property="notification_genre_code"
										value="${f:h(notification.notification_genre_code)}" />
									<html:hidden property="notification_title"
										value="${f:h(notification.notification_title)}" />
									<html:hidden property="notification_content"
										value="${f:h(notification.notification_content)}" />
									<html:hidden property="published_day"
										value="${f:h(notification.published_day)}" />
									<html:hidden property="published_flg"
										value="${f:h(notification.published_flg)}" />
									<td><s:link
											href="notification_upd?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
									&published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">編集</s:link></td>
									<td><s:link
											href="notification_del?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
									&published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">削除</s:link></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>