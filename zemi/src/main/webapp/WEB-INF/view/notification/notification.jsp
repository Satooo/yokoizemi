<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html ng-app="myApp">
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
	$(function() {
		var $setElm = $('td a');
		var cutFigure = '10'; // カットする文字数
		var afterTxt = ' …'; // 文字カット後に表示するテキスト

		$setElm.each(function() {
			var textLength = $(this).text().length;
			var textTrim = $(this).text().substr(0, (cutFigure))

			if (cutFigure < textLength) {
				$(this).html(textTrim + afterTxt).css({
					visibility : 'visible'
				});
			} else if (cutFigure >= textLength) {
				$(this).css({
					visibility : 'visible'
				});
			}
		});
	});
</script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>お知らせ</h3>
				<html:errors />
				<s:form method="POST">
					<table class="dataTable" id="syain_table">
						<thead>
							<tr>
								<th>掲載日</th>
								<th>ジャンル</th>
								<th>お知らせタイトル</th>
								<th>お知らせ内容</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="notification" varStatus="c"
								items="${notificationList}">
								<tr>
									<td>${f:h(notification.published_day)}</td>
									<td>${f:h(notification.notification_genre_codeLabel)}</td>
									<td><s:link
											href="notification_detail?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
											&published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">${f:h(notification.notification_title)}</s:link></td>
									<td><s:link
											href="notification_detail?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
											&published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">${f:h(notification.notification_content)}</s:link></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>