<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
					<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>お知らせ新規登録</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable" id="syain_table">
						<tr>
							<th>ジャンル</th>
							<td><html:select property="notification_genre_code"
									styleClass="tblInput">
									<html:option value="01">事務</html:option>
									<html:option value="02">教務</html:option>
									<html:option value="03">奨学金</html:option>
									<html:option value="09">その他</html:option>
								</html:select></td>
						</tr>
						<tr>
							<th>タイトル</th>
							<td><html:text property="notification_title" size="47"
									styleClass="tblInput" value="" /></td>
						</tr>
						<tr>
							<th>内容</th>
							<td><html:textarea property="notification_content" cols="47"
									rows="10" styleClass="tblInput" value="" /></td>
						</tr>
					</table>
					<br>
					<div Align="center">
						<s:submit property="notification_ins_fin" value="登録"
							styleClass="button" />
						<span style="margin-right: 1em;"></span>
						<s:submit property="notification_admin" value="戻る"
							styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>