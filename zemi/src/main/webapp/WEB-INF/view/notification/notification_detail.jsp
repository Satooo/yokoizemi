<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
					<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>お知らせ詳細</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable" id="syain_table">
					<tr>
							<th>掲載日</th>
							<td>${f:h(published_day)}</td>
						</tr>
						<tr>
							<th>タイトル</th>
							<td>${f:h(notification_title)}</td>
						</tr>
						<tr>
							<th>内容</th>
							<td>${f:h(notification_content)}</td>
						</tr>
					</table>
					<br>
					<div Align="center">
						<s:submit property="notification" value="お知らせ一覧へ"
							styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>