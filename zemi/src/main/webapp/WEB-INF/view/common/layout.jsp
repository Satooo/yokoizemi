<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
 <title><tiles:getAsString name="title" /></title>
 <link rel="stylesheet" href="${f:url('/css/common.css')}">
</head>
<body>
 <tiles:insert page="/WEB-INF/view/common/header.jsp"/>
 <tiles:insert page="/WEB-INF/view/common/menu.jsp"/>
 <tiles:insert page="/WEB-INF/view/common/sidemenu.jsp"/>
 <tiles:insert attribute="content"/>
 <tiles:insert page="/WEB-INF/view/common/footer.jsp"/>
</body>
</html>