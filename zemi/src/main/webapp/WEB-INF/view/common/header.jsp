    <!-- header要素は、ヘッダを表す -->
    <header>
        <s:link href="/menu"><img src="${f:url('/images/HCSLogo.png')}" alt="(株)HCS" id="logo" /></s:link>
        <small>学生交流率業界No1</small>
    <div id="contents">
    <p class="loginId">あなたのログインIDは<strong>${f:h(userDataDto.getUserId())}</strong></p>
    <s:link href="/logout"><html:img src="${f:url('/images/btn_base_brwn2.png')}" alt="ログアウト" styleId="logout"/></s:link>
    </div>
    <h1>HCS学生専用サイト</h1>
    </header>