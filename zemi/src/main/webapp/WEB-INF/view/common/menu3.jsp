<!-- Static navbar -->
<div class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<a class="navbar-brand">HCS学生専用サイト</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><s:link href="/menu3">
						<i class="fa fa-home"></i> ホーム  <span
							class="glyphicon glyphicon-home" aria-hidden="true"></span>
					</s:link></li>
				<li class="dropdown"><s:link href="/kyoumu">
						<i class="fa fa-info"></i> 教務部<span class="caret"></span>
					</s:link>
					<ul class="dropdown-menu">
						<li><s:link href="/kyoumu/teacherInfo">教員情報</s:link></li>
						<li><s:link href="/kyoumu/schedule">講義予定表</s:link></li>
						<li><s:link href="/kyoumu/syllabusInfo">シラバス</s:link></li>
						<li><s:link href="/kyoumu/demand">要望フォーム</s:link></li>
					</ul></li>
				<li class="dropdown"><s:link href="/student/student">
						<i class="fa fa-jpy"></i> 学生総合<span class="caret"></span>
					</s:link>
					<ul class="dropdown-menu">
						<li><s:link href="/student/srch">サークル情報</s:link></li>
						<li><s:link href="/student/publisher">広報</s:link></li>
						<li><s:link href="/student/lunch">学食情報</s:link></li>
					</ul></li>
				<li class="dropdown"><s:link href="/zimu/zimu">
						<i class="fa fa-jpy"></i> 事務部<span class="caret"></span>
					</s:link>
					<ul class="dropdown-menu">
						<li><s:link href="/zimu/jobInfo">事務バイト情報</s:link></li>
						<li><s:link href="/zimu/productInfo">事務商品情報</s:link></li>
						<li><s:link href="/zimu/shogakuInfo">奨学金情報</s:link></li>
					</ul></li>

				<li><s:link href="/gaibulink/link">
						<i class="fa fa-comments-o"></i> 外部リンク</s:link></li>
				<li><s:link href="/bbs/bbs">
						<i class="fa fa-comments-o"></i>
						掲示板</s:link></li>
				<li><s:link href="/kanri/kanri">管理</s:link></li>

			</ul>
			<h6 class="navbar-text">
				ようこそ${f:h(userDataDto.getUserId())} さん。
			</h6>
			<s:link styleClass="btn btn-default navbar-btn navbar-right"
				href="/logout">ログアウト</s:link>

		</div>
		<!--/.nav-collapse -->
	</div>
</div>