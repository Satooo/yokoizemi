
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- BootstrapのCSS読み込み -->
<link href="${f:url('/css/bootstrap.css')}" rel="stylesheet">
<link href="${f:url('/css/custom.css')}" rel="stylesheet">
<!-- jQuery読み込み -->
<script src="${f:url('/js/jquery-3.1.1.min.js')}"></script>
<!-- BootstrapのJS読み込み -->
<script src="${f:url('/js/bootstrap.js')}"></script>

<title><tiles:getAsString name="title" /></title>

</head>
<body>
	<tiles:insert page="/WEB-INF/view/common/menu3.jsp" />
	<tiles:insert page="/WEB-INF/view/common/sidemenu3.jsp">
		<div class="col-md-9 col-md-push-3">
			<tiles:insert attribute="content" />
		</div>
	</tiles:insert>
	<tiles:insert page="/WEB-INF/view/common/footer.jsp" />

</body>
</html>