<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<!-- aside要素はサイドメニューを表す -->
<aside>
	<h3>お知らせ</h3>
	<ul>
		<c:forEach var="notification" begin="0" end="3" varStatus="c"
			items="${notificationSideList}">
			<html:hidden property="notification_code" name="notification_code"
				value="${f:h(notification.notification_code)}" />
			<html:hidden property="notification_genre_code"
				value="${f:h(notification.notification_genre_code)}" />
			<html:hidden property="notification_title" name="notification_title"
				value="${f:h(notification.notification_title)}" />
			<html:hidden property="notification_content"
				value="${f:h(notification.notification_content)}" />
			<html:hidden property="published_day"
				value="${f:h(notification.published_day)}" />
			<html:hidden property="published_flg"
				value="${f:h(notification.published_flg)}" />
			<li><s:link
					href="/notification/notification_detail?notification_code=${f:h(notification.notification_code)}&notification_genre_code=${f:h(notification.notification_genre_code)}&notification_title=${f:h(notification.notification_title)}&notification_content=${f:h(notification.notification_content)}
				&published_day=${f:h(notification.published_day)}&published_flg=${f:h(notification.published_flg)}">
				${f:h(notification.notification_title)}
				</s:link></li>
		</c:forEach>
		<li><s:link href="/notification/notification">さらに詳しく...</s:link></li>
	</ul>
	<h3>過去のお知らせ</h3>
	
	<ul>
		<li><s:link href="/notification/notification1"><%Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    DateFormat df = new SimpleDateFormat("yyyy年MM月");
	    String day = df.format(date);
	    cal.add(Calendar.MONTH, -1);
        out.println(df.format(cal.getTime()));%></s:link></li>
		<li><s:link href="/notification/notification2">
		<%Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    DateFormat df = new SimpleDateFormat("yyyy年MM月");
	    String day = df.format(date);
	    cal.add(Calendar.MONTH, -2);
        out.println(df.format(cal.getTime()));%></s:link></li>
		<li><s:link href="/notification/notification3"><%Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    DateFormat df = new SimpleDateFormat("yyyy年MM月");
	    String day = df.format(date);
	    cal.add(Calendar.MONTH, -3);
        out.println(df.format(cal.getTime()));%></s:link></li>
      <li><s:link href="/notification/notification4"><%Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    DateFormat df = new SimpleDateFormat("yyyy年MM月");
	    String day = df.format(date);
	    cal.add(Calendar.MONTH, -4);
        out.println(df.format(cal.getTime()));%></s:link></li>
        <li><s:link href="/notification/notification5"><%Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    DateFormat df = new SimpleDateFormat("yyyy年MM月");
	    String day = df.format(date);
	    cal.add(Calendar.MONTH, -5);
        out.println(df.format(cal.getTime()));%></s:link></li>
	</ul>
</aside>