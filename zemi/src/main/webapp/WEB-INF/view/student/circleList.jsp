
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>学生総合トップページ</title>
</head>
<body>
  <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
  <tiles:put name="title" value="Hello SAStruts!"/>
  <tiles:put name="content" type="string">
  <article>
    <h3>学生総合トップページ</h3>
    <!-- コンテンツ（中央と右の2カラム） -->
    <div id="content">
<div id="news">
<h2>サークル活動</h2>
<p style="text-indent:2em">
HCSサークル一覧<p> 


<table border="1">
<tr><th>サークル名   </th><th> 顧問   </th><th> 活動概要  </th></tr>

<c:forEach var="circle" varStatus="c"
                                items="${circleList}">

      <tr>

    <td><a href="/zemi/student/circle_detail"target="_self">${f:h(circle.circle_name)}</a></td>

    <td>${f:h(circle.teacher_id)}</td>
    <td>${f:h(circle.circle_content)}</td>
    </tr>

    
                                </c:forEach>

</table>
</div>
</div>

            <br>
            <small>※××××年××月××日現在登録されているサークル一覧です</small>
            <html:errors/>
            <s:form method="POST">
            <br>
            </s:form>
            
   <a href="/zemi/student/student">戻る</a>
  </article>
</tiles:put>
</tiles:insert>
</body>
</html>