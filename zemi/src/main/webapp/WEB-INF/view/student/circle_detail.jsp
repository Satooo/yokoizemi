
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>学生総合トップページ</title>
</head>
<body>
  <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
  <tiles:put name="title" value="Hello SAStruts!"/>
  <tiles:put name="content" type="string">
  <article>
    <h3>学生総合トップページ</h3>
    <!-- コンテンツ（中央と右の2カラム） -->
    <div id="content">
<div id="news">
<h2>サークル活動</h2>
<p style="text-indent:2em">
HCSサークル一覧<p> 


<table border="1">
<tr><th>サークル名   </th><th>  活動内容</th></tr>

<tr>
    <td>野球サークル</td>

    <td>"毎月、４～５回のペースで練習および練習試合を行う。
北海道専門学校野球連盟に加盟し、専門学校の大会に参加する。
札幌市の軟式野球大会に参加する。"
    </td>
  </tr>
  <tr>
  <td>サッカーサークル</td>
     <td>"月に２回程度、札幌市産業振興センター等の体育館を借り、フットサルを行います。
また年に２回、専門学校のフットサル大会に出場します。"
     </td>

  </tr>

  <tr>
    <td>バスケットボールサークル</td>

     <td>"月に２回程度、札幌市産業振興センター等の体育館を借り、バスケットボールを行います。
今年度は大会などの参加予定はないですが、他専門学校と交流を深めるため練習試合を組む予定です。"
     </td>

  </tr>


  <tr>
    <td>テニスサークル</td>

     <td>"放課後、または休日に硬式テニスを行います。
活動場所は主に、学校近くの白石公園内テニスコートや豊平川河川敷コートの予定です。"
     </td>

<tr>
    <td>スカッシュサークル</td>
  
    <td>"他校のサークル参加者と一緒に練習を行う。
各種大会に参加し、全国大会出場を目指していく。
（平成２７年全国大会出場）"</td>
  </tr>


<tr>
    <td>クリエイタ（デザイン）サークル</td>

    <td>"デザイン系作品の企画から制作までの一連の活動を行う。
・クライアントへのヒアリング
・企画書の作成、提案
・作品の作成
・保守、管理のサポート
学園祭のポスター、パンフレットを作成する。
デザイン技術の伝承を行う。"</td>
</tr>
<tr>
    <td>クリエイタ（総合開発）サークル</td>
    <td>"コンテンツの企画から制作までの一連の活動を行う。
・企画書の作成、提案
・コンテンツ制作
・コンテストへの応募、プレゼンテーションスキルアップ
プログラミング勉強会やワークショップの開催。
サークル活動状況の発信。"</td>
  </tr>
  

<tr>
    <td>着付けサークル</td>
 
    <td>"定期的に、着付けを行う。
就職活動に役立ちそうなマナーや、姿勢についての簡単な指導をする。
あわせて女子学生の居場所を提供する。"</td>
  </tr>

<tr>
    <td>鉄道</td>

    <td>"「学生の、学生による、学生のための勉強会」を
モットーに、授業＋αの技術や最新技術を使ったシステム・アプリの開発を行う。"</td>
  </tr>

<tr>
    <td>軽音楽サークル「重低音」</td>

    <td>"月2回程度、校外スタジオにて練習
H29年度学園祭での演奏披露を目指す。"</td>
  </tr>


</table>
</div>
</div>

            <br>
            <small>※××××年××月××日現在登録されているサークル一覧です</small>
            <html:errors/>
            <s:form method="POST">
            <br>
            </s:form>
            
   <a href="/zemi/student/student">戻る</a>
  </article>
</tiles:put>
</tiles:insert>
</body>
</html>