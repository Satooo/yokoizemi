<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>学生総合トップページ</title>
</head>
<body>
  <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
  <tiles:put name="title" value="Hello SAStruts!"/>
  <tiles:put name="content" type="string">
  <article>
    <h3>学生総合トップページ</h3>
    <h2>学生総合</h2>
    <html:errors/>
	<s:form method="POST" action="/student">
        <table id="DVDinfo" class="basetable">
	        <thead>
	     		<tr>
	     		<th><s:submit value="サークル活動" property="srch" styleClass="btn3" /></th>
	     			<td>サークル情報を表示します。</td>
	    		</tr>
	    		<tr>
	    		<th><s:submit value="広報の情報" property="publisher" styleClass="btn3" /></th>
	     			<td>広報の情報を表示します。</td>
	    		</tr>
	    		<tr>
	    		<th><s:submit value="学食情報" property="lunch" styleClass="btn3" /></th>
	     			<td>学食のメニューを掲載しています。</td>
	    		</tr>
	    		
	
	    	</thead>
    	</table>
    <br>
    <br>
    
    </s:form>
    
  </article>
</tiles:put>
</tiles:insert>
</body>
</html>