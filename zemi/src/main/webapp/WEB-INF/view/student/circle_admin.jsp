<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="${f:url('/css/common.css')}" />
<link rel="stylesheet" href="${f:url('/css/jquery.dataTables.min.css')}" />
<script src="${f:url('/js/jquery-1.9.1.min.js')}"></script>
<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
<script src="${f:url('/js/dataTable_sub.js')}"></script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>サークル管理者用</h3>
				<html:errors />
				<s:form method="POST" >
					<div Align="right">
						<s:link href="part_ins">新規作成</s:link>
					</div>
					<table class="dataTable" id="syain_table">
						<thead>
							<tr>
								<th>サークル名</th>
								<th>顧問</th>
								<th>活動場所</th>
								<th>削除</th>
							</tr>
						</thead>
						<tbody>
                            <c:forEach var="circle" varStatus="c" items="${circleList}">
      <tr>                        
    <td>${f:h(circle.circle_name)}</td>
    <td>${f:h(circle.teacher_id)}</td>
    <td>${f:h(circle.circle_content)}</td>

                                    <html:hidden property="circle_code" value="${f:h(circle.circle_code)}" />
                                    <html:hidden property="circle_name" value="${f:h(circle.circle_name)}" />
                                    <html:hidden property="teacher_id" value="${f:h(circle.teacher_id)}" />
                                    <html:hidden property="circle_content" value="${f:h(circle_content)}" />
                                    <td><s:link 
                                    href="part_del?part_code=${f:h(part.part_code)}&file_name=${f:h(part.file_name)}&conditions=${f:h(part.conditions)}
                                    &deadline=${f:h(part.deadline)}&published_flg=${f:h(part.published_flg)}">削除</s:link></td>
                                </tr>
                            </c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>