<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>広報</title>
</head>
<body>
  <tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
  <tiles:put name="title" value="Hello SAStruts!"/>
  <tiles:put name="content" type="string">
  <article>
    <h2>広報ページ</h2>

<script type="text/javascript">

window.onload = function (){
    setEvent_to_OBJ("document.getElementById('year')" ,"change","iniD") 
    setEvent_to_OBJ("document.getElementById('month')","change","iniD") 
    ini();
}
function setEvent_to_OBJ(objNameStr,eventTypeNameStr,fncNameStr){
    eval(objNameStr+".on"+eventTypeNameStr+"="+ fncNameStr);
}

arrM = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31);
var dayT = new Date;
var strY = dayT.getYear();
var strM = dayT.getMonth();
var strD = dayT.getDate();
if(strY < 1900){strY += 1900;}//NN系対策

function ini(){
    iniY();
    iniM();
    iniD();
}
function iniY(){
    var yr = document.getElementById("year");
    yr.options.length = 3;
    yr.options[0].text  = strY - 1;
    yr.options[1].text  = strY;
    yr.options[2].text  = strY + 1;
    yr.options[0].value = strY - 1;
    yr.options[1].value = strY;
    yr.options[2].value = strY + 1;
    yr.options[1].selected = "selected";
}
function iniM(){
    var mt = document.getElementById("month");
    mt.options.length = 12;
    for(var i=0;i<12;i++){
        mt.options[i].text  = i + 1;
        mt.options[i].value = i + 1;
        if(i == strM){mt.options[i].selected = "selected";}
    }
}
function iniD(){
    var yr    = document.getElementById("year");
    var dt    = document.getElementById("date");
    var mt    = document.getElementById("month");
    var u_flg = 0;//うるう年か否か
    var d_flg = 0;//年月が現在と同じか否か
    //月末の日数
    var lngD  = arrM[mt.options[mt.selectedIndex].value];
    if(mt.options[mt.selectedIndex].value == 2){
        u_flg = uruu(yr.options[yr.selectedIndex].value);
    }
    if(u_flg != 0){lngD ++;}
    if(mt.options[mt.selectedIndex].value == strM + 1 &&
        yr.options[yr.selectedIndex].value == strY){d_flg = 1;}

    dt.options.length = lngD;
    for(var i=0;i<lngD;i++){
        dt.options[i].text  = i + 1;
        dt.options[i].value = i + 1;
        if(i == strD - 1 && d_flg == 1){
            dt.options[i].selected = "selected";
        }
    }
    if(d_flg == 0){
        dt.options[0].selected = "selected";
    }
}
function uruu(year){
    if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
        return 1;
    }else{
        return 0;
    }
}

</script>

<form action="" method="post">
    <select name="year" id="year"><option value="">年</option></select>
    <select name="month" id="month"><option value="">月</option></select>

    <input type="submit" value="検索">
</form>
    <s:form method="POST" action="/student">
 <table id="DVDinfo" class="basetable">
        <thead>
            <tr>
            <th>広報</th>
       <tr>
               
                 <td><a href="${f:url(pass)}">HCSニュース全体</a></td>
           </tr>
           <tr>     
             
               <td><a href="${f:url(pass1)}">HCSニュース表(1/4)</a></td>
            </tr>
            
            <tr>  
                     
                         <td><a href="${f:url(pass2)}">HCSニュース表(2/4)</a></td>
             </tr>
             <tr>
                             <td><a href="${f:url(pass3)}">HCSニュース裏(3/4)</a></td>
             </tr>
             <tr>
                               <td><a href="${f:url(pass4)}">HCSニュース裏(4/4)</a></td>
            </tr>
            </thead>
            </table>
            <br>
            <small>※PDFの情報を閲覧するためには、Adobe Readerが必要です。</small>
            <html:errors/>
            <s:form method="POST">
            
            <br>
</s:form>
            </s:form>

   <a href="/zemi/student/student">戻る</a>

   
  </article>
</tiles:put>
</tiles:insert>
</body>
</html>