<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>学生総合 - 学食メニュ一覧</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>学食メニュー</h3>
				<table class="dataTable" id="syain_table">
					<tr align="center">
						<td><img src="${f:url('/images/food_curry.png')}" width="128"
							height="128" alt="curry" /></td>
						<td><img src="${f:url('/images/food_katsudon.png')}"
							width="128" height="128" alt="katsudon" /></td>
						<td><img src="${f:url('/images/food_ramen.png')}" width="128"
							height="128" alt="ramen" /></td>
					</tr>
					<tr align="center">
						<td>カレーライス</td>
						<td>カツ丼</td>
						<td>ラーメン</td>
					</tr>
					<tr align="center">
						<td>350円</td>
						<td>400円</td>
						<td>味噌　380円<br>醤油・塩　350円</td>
					</tr>
					<tr align="center">
						<td><img src="${f:url('/images/food_soba.png')}" width="128"
							height="128" alt="soba" /></td>
						<td><img src="${f:url('/images/food_udon.png')}" width="128"
							height="128" alt="udon" /></td>
						<td><img src="${f:url('/images/food_teisyoku.png')}"
							width="128" height="128" alt="udon" /></td>
					</tr>
					<tr align="center">
						<td>うどん</td>
						<td>そば</td>
						<td>日替わり定食A・B</td>
					</tr>
					<tr align="center">
						<td>210円</td>
						<td>210円</td>
						<td>A.450円 B.500円</td>
					</tr>
				</table>
				<html:errors />

			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>