<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>教務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>教務部</h3>
				<!-- 現在の日付から先月、今月、来月を呼ぶ処理が必要 -->
				<h2>
					講義予定
					<s:form method="POST" action="schedule">

						<html:select property="scheduleselectcourse">
							<option value=>学科を選択してください</option>
							<option value="情報大学併学科">情報大学併学科</option>
							<option value="システムエンジニア科">システムエンジニア科</option>
							<option value="プログラマ科">プログラマ科</option>
							<option value="ゲームクリエイタ科">ゲームクリエイタ科</option>
							<option value="マルチメディアCG科">マルチメディアCG科</option>
							<option value="ビジネス科">ビジネス科</option>
							<option value="情報プロセス科">情報プロセス科</option>
							<option value="テクニカルエンジニア科">テクニカルエンジニア科</option>
							<option value="卒業ゼミ">卒業ゼミ(対象期間のみ)</option>
						</html:select>
						<html:select property="scheduleselectschoolyear">
							<option value=>学年を選択してください</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</html:select>

						<s:submit property="schedulu" value="検索" />
					</s:form>
				</h2>
				<table id="DVDinfo" class="basetable">
					<thead>

						<%-- <tr>
							<th>先月</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
						<tr>
							<th>今月</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
						<tr>
							<th>来月</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
--%>
							<%
								String[] stringArray = { "先月", "今月", "来月" };
											int i = 0;
							%>
							<c:forEach var="m" varStatus="c" items="${scheduleDtoList}">
								<tr>
									<th><%=stringArray[i++]%></th>
									<td><s:submit value="pdfファイルを開く" property="download"
											styleClass="btn3" /></td>
									<html:hidden property="schedulescontent"
										value="${f:h(m.scheduleFile)}" />
								</tr>

							</c:forEach>

					</thead>
				</table>
				<br> <small>※PDFの情報を閲覧するためには、Adobe Readerが必要です。</small>
				<html:errors />
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>