<%--                                        --%>
<%--  index.jsp  トップページ                   --%>
<%--                                        --%>
<%--  @author  hcs                          --%>
<%--  @date    2016/04/25                   --%>
<%--                                        --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title>教務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCS学生交流支援システム" />
		<tiles:put name="content" type="string">
			<!-- article要素は本文(記事)を表す -->
			<article>
				<h3>トップページ</h3>

				<h1>
					<span style="border-bottom: solid 3px #000000;">要望フォーム確認</span>
				</h1>

				<table id="DVDinfo" class="basetable">

					<tr>
						<th>学籍番号</th>
						<td>システムエンジニア科 3年</td>
					</tr>
					<tr>
						<th>クラス学年名前</th>
						<td>システムエンジニア科 3年</td>
					</tr>
					<tr>
						<th>種別</th>
						<td>${f:h(demand_genre)}</td>
					</tr>
					<tr>
						<th>要望内容</th>
						<td>${f:br(f:nbsp(f:h(demand_content)))}</td>
					</tr>


				</table>

				<br>
				<%--完了画面に遷移 --%>
				<s:form method="POST" action="/kyoumu">
					<html:text style="display:none" property="demand_genre" />
					<html:text style="display:none" property="demand_content" />
					<Div Align="center">
						<s:submit property="kanryo" value="送信" styleClass="btn2"/>
					</Div>

				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>