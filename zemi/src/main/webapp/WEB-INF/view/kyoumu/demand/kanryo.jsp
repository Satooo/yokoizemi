<%--                                        --%>
<%--  index.jsp  トップページ                   --%>
<%--                                        --%>
<%--  @author  hcs                          --%>
<%--  @date    2016/04/25                   --%>
<%--                                        --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title>教務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCS学生交流支援システム" />
		<tiles:put name="content" type="string">
			<!-- article要素は本文(記事)を表す -->
			<article>
				
				<h3>トップページ</h3>

				<h1>
					<span style="border-bottom: solid 3px;">要望フォーム完了</span>
				</h1>
				<table id="DVDinfo" class="basetable">
					<tr>
						<td>${f:h(message)}</td>
					</tr>
				</table>
				<br>
				<div class="center">
					<%--zimu.jspに遷移 --%>
					<s:submit value="戻る" styleClass="btn2" onclick="history.go(-3)" />
				</div>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>