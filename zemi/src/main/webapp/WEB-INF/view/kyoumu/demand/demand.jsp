<%--                                        --%>
<%--  index.jsp  トップページ                   --%>
<%--                                        --%>
<%--  @author  hcs                          --%>
<%--  @date    2016/04/25                   --%>
<%--                                        --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title>教務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="HCS学生交流支援システム" />
		<tiles:put name="content" type="string">
			<!-- article要素は本文(記事)を表す -->
			<article>
				<h3>教務部</h3>
				<h2>要望</h2>
				<s:form method="POST" action="/kyoumu">
					<table id="DVDinfo" class="basetable">
						<thead>
							<tr>
								<th>学科・学年</th>
								<td>システムエンジニア科 3年</td>
							</tr>
							<tr>
								<th>要望ジャンル</th>
								<td><html:select style="width: 200px"
										property="demand_genre">
										<option value="科目関係">選択してください</option>
										<option value="映画鑑賞">映画鑑賞</option>
										<option value="運動会">運動会</option>
										<option value="学校祭">学校祭</option>
										<option value="体育">体育</option>
										<option value="その他">その他</option>
									</html:select></td>
							</tr>
							<tr>
								<th>メッセージ</th>
								<td><html:textarea property="demand_content" rows="7"
										cols="50"></html:textarea></td>
							</tr>

						</thead>
					</table>

					<br>
						<!-- 確認画面に遷移する -->
						<div Align="center">
						<s:submit property="kakunin" value="確認" styleClass="button"/>
                        <s:submit property="part_admin" value="戻る" styleClass="button" />
                    </div>
					</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>