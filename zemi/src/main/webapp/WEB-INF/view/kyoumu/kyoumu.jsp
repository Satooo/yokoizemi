<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>教務トップページ</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>教務部</h3>
				<h2>教務</h2>
				<s:form method="POST">
				<table id="DVDinfo" class="basetable">

					<thead>
						

							<tr>
								<th><s:submit value="教員情報" property="teacherInfo"
										styleClass="btn3" /></th>
								<td>教員情報を表示します。</td>
							</tr>
							<tr>
								<th><s:submit value="シラバス情報" property="syllabusInfo"
										styleClass="btn3" /></th>
								<td>学科ごとに今年度のシラバスを表示します。</td>
							</tr>

							<tr>
								<th><s:submit value="講義予定表" property="scheduleInfo"
										styleClass="btn3" /></th>
								<td>学科ごとに3ヶ月分の講義予定表を表示します。</td>
							</tr>

							<tr>
								<th><s:submit value="要望フォーム" property="demand"
										styleClass="btn3" /></th>
								<td>要望してください。</td>
							</tr>
						


					</thead>

				</table>
				</s:form>
				<html:errors />

				<br> <br>

			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>