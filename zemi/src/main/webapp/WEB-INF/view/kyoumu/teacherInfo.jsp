<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>事務・教務 - シラバス</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="教員情報" />
		<tiles:put name="content" type="string">
			<article>
				<h3>教務部</h3>
				<!-- 現在の日付から先月、今月、来月を呼ぶ処理が必要 -->
				<h2>

					教員情報
					<s:form method="POST" action="teacherInfo">
						<html:select property="teacherselectcourse">
							<option value=>選択してください</option>
							<option value="情報大学併学科">情報大学併学科</option>
							<option value="システムエンジニア科">システムエンジニア科</option>
							<option value="プログラマ科">プログラマ科</option>
							<option value="ゲームクリエイタ科">ゲームクリエイタ科</option>
							<option value="マルチメディアCG科">マルチメディアCG科</option>
							<option value="ビジネス科">ビジネス科</option>
							<option value="情報プロセス科">情報プロセス科</option>
							<option value="テクニカルエンジニア科">テクニカルエンジニア科</option>
						</html:select>

						<s:submit value="検索" styleClass="btn2" />
					</s:form>
				</h2>
				<h4>${f:h(teacherselectcourse)}</h4>
				
				
				<table id="DVDinfo" class="basetable2">
					<thead>
						<tr>
							<td>学年</td>
							<td>写真</td>
							<td>名前</td>
							<td>担当サークル</td>
						</tr>
						
						
						<c:forEach var="m" varStatus="c" items="${userDtoList}">
							<tr>
								<th>${f:h(m.classcode)}</th>
								<td><img src="${f:url('/images/teacher.jpg')}" alt="(株)HCS"
									id="logo" /></td>
								<td>${f:h(m.userName)}</td>
								<td>${f:h(m.circle)}</td>
								<td></td>
								<td>test</td>
							</tr>
						</c:forEach>

					</thead>
				</table>
				<html:errors />
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>