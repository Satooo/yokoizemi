<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>教務部</title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>教務部</h3>
				<h2>
					シラバス
					<s:form method="POST" action="syllabusInfo">
						<html:select property="syllabusselectcourse">
							<option value=>選択してください</option>
							<option value="情報大学併学科">情報大学併学科</option>
							<option value="システムエンジニア科">システムエンジニア科</option>
							<option value="プログラマ科">プログラマ科</option>
							<option value="ゲームクリエイタ科">ゲームクリエイタ科</option>
							<option value="マルチメディアCG科">マルチメディアCG科</option>
							<option value="ビジネス科">ビジネス科</option>
							<option value="情報プロセス科">情報プロセス科</option>
							<option value="テクニカルエンジニア科">テクニカルエンジニア科</option>
						</html:select>
						<s:submit value="検索" styleClass="btn2" />
					</s:form>
				</h2>
				<table id="DVDinfo" class="basetable">
					<thead>    <%--
						<tr>
							<th>1年次</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
						<tr>
							<th>2年次</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
						<tr>
							<th>3年次</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr>
						<tr>
							<th>4年次</th>
							<td><a
								href="http://dl2.dl.multidevice-disc.com/dl/5945-a7369411063b88037b39aa38617f01a0">PDFファイルを開く</a>
							</td>
						</tr> --%>
						<%
							int i = 1;
						%>
						<c:forEach var="m" varStatus="c" items="${syllabusDtoList}">
							<tr>
								<th><%=i++%>年次</th>
								<%--
								<th>${f:h(m.syllabusFile)}</th> 
								--%>
								<th>${f:h(m.syllabusCode)}</th> 

							</tr>
						</c:forEach>
					</thead>
				</table>
				<br> <small>※PDFの情報を閲覧するためには、Adobe Readerが必要です。</small>
				<html:errors />
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>