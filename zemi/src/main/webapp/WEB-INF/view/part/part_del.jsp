<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>


</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">

			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>バイト削除</h3>
				<html:errors />
				<s:form method="POST">
					<html:hidden property="part_code" value="${f:h(part_code)}" />
					<html:hidden property="file_name" value="${f:h(file_name)}" />
					<html:hidden property="conditions" value="${f:h(conditions)}" />
					<html:hidden property="deadline" value="${f:h(deadline)}" />
					<html:hidden property="published_flg" value="${f:h(published_flg)}" />
					<table class="basetable" id="syain_table">
						<tr>
							<th>タイトル</th>
							<td>${f:h(file_name)}</td>
						</tr>
						<tr>
							<th>条件</th>
							<td>${f:h(conditions)}</td>
						</tr>
						<tr>
							<th>〆切日時</th>
							<td>${f:h(deadline)}</td>
						</tr>
					</table>
					<br>
					<div Align="center">
						<s:submit property="part_del_fin" value="削除" styleClass="button" />
						<s:submit property="part_admin" value="戻る" styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>