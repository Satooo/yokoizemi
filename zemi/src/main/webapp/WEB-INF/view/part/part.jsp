<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>

			<article>
				<h3>バイト一覧</h3>
				<html:errors />
				<s:form method="POST">
					<div Align="right">
						<p>気になったバイトがあった場合は本校舎1階事務までお越し下さい。</p>
					</div>
					<table class="table table-striped" id="syain_table">
						<thead>
							<tr>
								<th>タイトル</th>
								<th>条件</th>
								<th>〆切日時</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="part" varStatus="c" items="${partList}">
								<tr>
									<td>${f:h(part.file_name)}</td>
									<td>${f:h(part.conditions)}</td>
									<td>${f:h(part.deadline)}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>