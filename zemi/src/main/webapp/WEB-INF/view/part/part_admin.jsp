<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>
			<article>
				<h3>バイト管理者用</h3>
				<html:errors />
				<s:form method="POST">
					<div Align="right">
						<s:link href="part_ins">新規作成</s:link>
					</div>
					<table class="dataTable" id="syain_table">
						<thead>
							<tr>
								<th>タイトル</th>
								<th>募集条件</th>
								<th>〆切日時</th>
								<th>削除</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="part" varStatus="c" items="${partList}">
								<tr>
									<td>${f:h(part.file_name)}</td>
									<td>${f:h(part.conditions)}</td>
									<td>${f:h(part.deadline)}</td>
									<html:hidden property="part_code"
										value="${f:h(part.part_code)}" />
									<html:hidden property="file_name"
										value="${f:h(part.file_name)}" />
									<html:hidden property="conditions"
										value="${f:h(part.conditions)}" />
									<html:hidden property="deadline" value="${f:h(part.deadline)}" />
									<html:hidden property="published_flg"
										value="${f:h(part.published_flg)}" />
									<td><s:link
											href="part_del?part_code=${f:h(part.part_code)}&file_name=${f:h(part.file_name)}&conditions=${f:h(part.conditions)}
                                    &deadline=${f:h(part.deadline)}&published_flg=${f:h(part.published_flg)}">削除</s:link></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>