<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="${f:url('/css/common.css')}" />
<link rel="stylesheet" href="${f:url('/css/jquery.dataTables.min.css')}" />
<script src="${f:url('/js/jquery-1.9.1.min.js')}"></script>
<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
<script src="${f:url('/js/dataTable_sub.js')}"></script>
</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<article>
				<h3>バイト更新</h3>
				<html:errors />
				<s:form method="POST">
					<html:hidden property="part_code" value="${f:h(part_code)}" />
					<html:hidden property="file_name" value="${f:h(file_name)}" />
					<html:hidden property="conditions" value="${f:h(conditions)}" />
					<html:hidden property="deadline" value="${f:h(deadline)}" />
					<html:hidden property="published_flg" value="${f:h(published_flg)}" />
					<table class="basetable" id="syain_table">
						<tr>
							<th>タイトル</th>
							<td><html:text property="file_name"
									styleClass="tblInput" value="${f:h(file_name)}" /></td>
						</tr>
						<tr>
                            <th>募集条件</th>
                            <td><html:text property="conditions"
                                    styleClass="tblInput" value="${f:h(conditions)}" /></td>
                        </tr>
						<tr>
							<th>〆切日時</th>
							<td><html:textarea property="deadline" cols="47"
									rows="10" styleClass="tblInput"
									value="${f:h(deadline)}" /></td>
						</tr>
					</table>
					<br>
					<div Align="center">
						<s:submit property="part_upd_fin" value="更新"
							styleClass="button" />
						<s:submit property="part_admin" value="戻る"
							styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>