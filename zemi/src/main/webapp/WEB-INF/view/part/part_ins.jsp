<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>


<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css">


</head>
<body>
	<tiles:insert template="/WEB-INF/view/common/layout3.jsp" flush="true">
		<tiles:put name="title" value="Hello SAStruts!" />
		<tiles:put name="content" type="string">
			<link rel="stylesheet"
				href="${f:url('/css/jquery.dataTables.min.css')}" />

			<script
				src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script
				src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
			<script
				src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
			<script>
				$(function() {
					$("#datepicker").datepicker();
				});
			</script>
			<script src="${f:url('/js/jquery.dataTables.min.js')}"></script>
			<script src="${f:url('/js/dataTable_sub.js')}"></script>

			<article>
				<h3>バイト新規登録</h3>
				<html:errors />
				<s:form method="POST">
					<table class="basetable">
						<tr>
							<th>タイトル</th>
							<td><html:text property="file_name" styleClass="tblInput"
									value="" /></td>
						</tr>
						<tr>
							<th>条件</th>
							<td><html:textarea property="conditions" cols="47" rows="10"
									styleClass="tblInput" value="" /></td>
						</tr>
						<tr>
							<th>〆切日時</th>
							<td><input type="text" id="datepicker" name="deadline"></td>

						</tr>

					</table>
					<br>
					<div Align="center">
						<s:submit property="part_ins_fin" value="登録" styleClass="button" />
						<s:submit property="part_admin" value="戻る" styleClass="button" />
					</div>
				</s:form>
			</article>
		</tiles:put>
	</tiles:insert>
</body>
</html>