		<?php
		header("Content-type: application/x-javascript");
		/* ドライバ呼び出しを使用して MySQL データベースに接続する */
		$dsn = 'mysql:dbname=yokoizemi;host=localhost';
		$user = 'user8';
		$password = 'yokoizemi';

		try {
		    $dbh = new PDO($dsn, $user, $password);
		} catch (PDOException $e) {
			/*  エラー表示 */
			/*  もし手抜きしたくない場合は普通にHTMLの表示を記述する */
		    echo 'Connection failed: ' . $e->getMessage();
		}

		header('Content-Type: text/html; charset=utf-8');

		/* SELECT文を変数に格納 */
		$sql = "SELECT * FROM response";
		/* SQLステートメントを実行し、結果を変数に格納 */
		$stmt = $dbh->query($sql);
		/* レスのインデックス */
		$resIndex = 1;

		/* foreach文で配列の中身を一行ずつ出力 */
		foreach ($stmt as $row) {

		  /* データベースのフィールド名で出力 */
			if ($row['ng_flag'] < 1) {
				$response = $row['response'];
				$resInfo = sprintf("%04s",  $resIndex). " : ". " ID: ".$row['response_id']." time: ".$row['posting_day']." name: ".
						$row['student_number'];
				/* 最終的にjavascriptの命令でhtmlに受け渡さなければいけないので
				 * document.write命令ごとecho出力*/
				echo "document.write(\"$resInfo\")";
				echo "document.write(\"<br />\")";
				echo "document.write(\"(htmlspecialchars($response, ENT_COMPAT, 'UTF-8').'<br>')\")";
			} else {
				echo sprintf("%04s",  $resIndex)." : ";
				echo "document.write(\"<i>#削除されました</i><br />\")";
				echo "document.write(\"<i>管理者により削除されました</i><br />\")";
			}
			$resIndex = $resIndex + 1;
		}
		echo "</table>";
		?>
