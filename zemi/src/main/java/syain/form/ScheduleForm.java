package syain.form;

import org.apache.struts.action.ActionForm;
import org.seasar.struts.annotation.Required;

public class ScheduleForm extends ActionForm {
	private static final long serialVersionUID = 1L;

	// セッターゲッター
	@Required
	public String scheduleselectcourse;

	@Required
	public String scheduleselectschoolyear;
}
