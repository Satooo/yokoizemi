package syain.form;

import org.apache.struts.action.ActionForm;
import org.seasar.struts.annotation.Required;

public class TeacherForm extends ActionForm {
	private static final long serialVersionUID = 1L;

	// セッターゲッター
	@Required
	public String teacherselectcourse;
	@Required
	public String teacherselectschoolyear;
	@Required
	public String result = "送信に失敗しました";

}
