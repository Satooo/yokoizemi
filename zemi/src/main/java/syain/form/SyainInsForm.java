package syain.form;

import org.seasar.struts.annotation.IntegerType;
import org.seasar.struts.annotation.Maxlength;

public class SyainInsForm {
	


	
	/* 1円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック(最大5桁)
	@IntegerType					// integerに変換可能か否か
	public String m1yen;
	
	/* 5円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m5yen;
	
	/* 10円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m10yen;
	
	/* 50円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m50yen;
	
	/* 100円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m100yen;
	
	/* 500円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m500yen;
	
	/* 1000円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m1000yen;
	
	/* 5000円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m5000yen;
	
	/* 10000円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m10000yen;
	
	/* 2000円 */
	@Maxlength(maxlength = 5)		// 最大文字数のチェック
	@IntegerType					// integerに変換可能か否か
	public String m2000yen;
	
	
	
}


