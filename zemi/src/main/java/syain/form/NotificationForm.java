package syain.form;


import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

public class NotificationForm {
	
	@Maxlength(maxlength=50)
	public String notification_code;
	
	@Maxlength(maxlength=50)
	public String notification_genre_code;
	
	@Required
	@Maxlength(maxlength=50)
	public String notification_title;
	
	@Required
	@Maxlength(maxlength=500)
	public String notification_content;
	
	@Maxlength(maxlength=50)
	public String published_day;
	
	@Maxlength(maxlength=50)
	public String published_flg;
}
