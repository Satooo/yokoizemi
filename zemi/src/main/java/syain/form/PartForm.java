package syain.form;

import org.seasar.struts.annotation.Maxlength;
import org.seasar.struts.annotation.Required;

public class PartForm {
	
	@Maxlength(maxlength=8)
	public String part_code;
	
	@Required
	@Maxlength(maxlength=40)
	public String file_name;
	
	@Required
	@Maxlength(maxlength=40)
	public String conditions;
	
	@Maxlength(maxlength=50)
	public String deadline;
	
	@Maxlength(maxlength=50)
	public String published_flg;

}
