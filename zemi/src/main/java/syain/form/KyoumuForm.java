package syain.form;

import org.apache.struts.action.ActionForm;
import org.seasar.struts.annotation.Required;

public class KyoumuForm extends ActionForm {
	private static final long serialVersionUID = 1L;

	// 教員検索学科
	@Required
	public String teacherselectcourse;
	// 教員検索学年
	@Required
	public String teacherselectschoolyear;

	// シラバス検索学科
	@Required
	public String syllabusselectcourse;

	// 講義予定検索学科
	@Required
	public String scheduleselectcourse;

	@Required
	public String schedulescontent;
	
	// 講義予定検索学年
	@Required
	public String scheduleselectschoolyear;

	@Required
	public String department_code;

	// セッターゲッター
	@Required
	public String demand_content;
	@Required
	public String demand_genre;
	@Required
	public String message = "送信に失敗しました";

	public void riset() {
		teacherselectcourse = "";
		teacherselectschoolyear = "";
		syllabusselectcourse = "";
		scheduleselectcourse = "";
		scheduleselectschoolyear = "";
		department_code = "";
		demand_content = "";
		demand_genre = "";
		message = "";
	}

}
