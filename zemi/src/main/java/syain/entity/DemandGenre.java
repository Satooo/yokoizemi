package syain.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * DemandGenreエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2016/12/21 11:10:22")
public class DemandGenre implements Serializable {

    private static final long serialVersionUID = 1L;

    /** demandGenreCodeプロパティ */
    @Id
    @Column(length = 2, nullable = false, unique = true)
    public String demandGenreCode;

    /** demandGenreNameプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String demandGenreName;

    /** demandList関連プロパティ */
    @OneToMany(mappedBy = "demandGenre")
    public List<Demand> demandList;
}