package syain.entity;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Part}のプロパティ名の集合です。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl" }, date = "2016/06/03 9:32:33")
public class PartNames {

	/**
	 * part_codeのプロパティ名を返します。
	 * 
	 * @return part_codeのプロパティ名
	 */
	public static PropertyName<String> part_code() {
		return new PropertyName<String>("part_code");
	}

	/**
	 * file_nameのプロパティ名を返します。
	 * 
	 * @return file_nameのプロパティ名
	 */
	public static PropertyName<String> file_name() {
		return new PropertyName<String>("file_name");
	}

	/**
	 * conditionsのプロパティ名を返します。
	 * 
	 * @return conditionsのプロパティ名
	 */
	public static PropertyName<String> conditions() {
		return new PropertyName<String>("conditions");
	}

	/**
	 * deadlineのプロパティ名を返します。
	 * 
	 * @return deadlineのプロパティ名
	 */
	public static PropertyName<String> deadline() {
		return new PropertyName<String>("deadline");
	}

	/**
	 * published_flgのプロパティ名を返します。
	 * 
	 * @return published_flgのプロパティ名
	 */
	public static PropertyName<String> published_flg() {
		return new PropertyName<String>("published_flg");
	}

	/**
	 * @author S2JDBC-Gen
	 */
	public static class _PartficationNames extends PropertyName<Part> {

		/**
		 * インスタンスを構築します。
		 */
		public _PartficationNames() {
		}

		/**
		 * インスタンスを構築します。
		 * 
		 * @param name
		 *            名前
		 */
		public _PartficationNames(final String name) {
			super(name);
		}

		/**
		 * インスタンスを構築します。
		 * 
		 * @param parent
		 *            親
		 * @param name
		 *            名前
		 */
		public _PartficationNames(final PropertyName<?> parent, final String name) {
			super(parent, name);
		}

		/**
		 * part_codeのプロパティ名を返します。
		 *
		 * @return part_codeのプロパティ名
		 */
		public PropertyName<String> part_code() {
			return new PropertyName<String>(this, "part_code");
		}

		/**
		 * file_nameのプロパティ名を返します。
		 *
		 * @return file_nameのプロパティ名
		 */
		public PropertyName<String> file_name() {
			return new PropertyName<String>(this, "file_name");
		}

		/**
		 * conditionsのプロパティ名を返します。
		 *
		 * @return conditionsのプロパティ名
		 */
		public PropertyName<String> conditions() {
			return new PropertyName<String>(this, "conditions");
		}

		/**
		 * deadlineのプロパティ名を返します。
		 *
		 * @return deadlineのプロパティ名
		 */
		public PropertyName<String> deadline() {
			return new PropertyName<String>(this, "deadline");
		}

		/**
		 * published_flgのプロパティ名を返します。
		 *
		 * @return published_flgのプロパティ名
		 */
		public PropertyName<String> published_flg() {
			return new PropertyName<String>(this, "published_flg");
		}

	}
}