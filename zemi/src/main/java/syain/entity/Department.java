package syain.entity;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Departmentエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/01/25 11:50:12")
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;

    /** departmentCodeプロパティ */
    @Id
    @Column(length = 2, nullable = false, unique = true)
    public String departmentCode;

    /** departmentNameプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String departmentName;
}