package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Notification}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2016/06/03 9:32:33")
public class NotificationNames {

    /**
     * notification_codeのプロパティ名を返します。
     * 
     * @return notification_codeのプロパティ名
     */
    public static PropertyName<String> notification_code() {
        return new PropertyName<String>("notification_code");
    }

    /**
     * notification_genre_codeのプロパティ名を返します。
     * 
     * @return notification_genre_codeのプロパティ名
     */
    public static PropertyName<String> notification_genre_code() {
        return new PropertyName<String>("notification_genre_code");
    }

    /**
     * notification_titleのプロパティ名を返します。
     * 
     * @return notification_titleのプロパティ名
     */
    public static PropertyName<String> notification_title() {
        return new PropertyName<String>("notification_title");
    }

    /**
     * notification_contentのプロパティ名を返します。
     * 
     * @return notification_contentのプロパティ名
     */
    public static PropertyName<String> notification_content() {
        return new PropertyName<String>("notification_content");
    }
    
    /**
     * published_dayのプロパティ名を返します。
     * 
     * @return published_dayのプロパティ名
     */
    public static PropertyName<String> published_day() {
        return new PropertyName<String>("published_day");
    }
    
    /**
     * published_flgのプロパティ名を返します。
     * 
     * @return published_flgのプロパティ名
     */
    public static PropertyName<String> published_flg() {
        return new PropertyName<String>("published_flg");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _NotificationNames extends PropertyName<Notification> {

        /**
         * インスタンスを構築します。
         */
        public _NotificationNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _NotificationNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _NotificationNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * notification_codeのプロパティ名を返します。
         *
         * @return notification_codeのプロパティ名
         */
        public PropertyName<String> notification_code() {
            return new PropertyName<String>(this, "notification_code");
        }

        /**
         * notification_genre_idのプロパティ名を返します。
         *
         * @return notification_genre_idのプロパティ名
         */
        public PropertyName<String> notification_genre_code() {
            return new PropertyName<String>(this, "notification_genre_code");
        }

        /**
         * notification_titleのプロパティ名を返します。
         *
         * @return notification_titleのプロパティ名
         */
        public PropertyName<String> notification_title() {
            return new PropertyName<String>(this, "notification_title");
        }

        /**
         * notification_contentのプロパティ名を返します。
         *
         * @return notification_contentのプロパティ名
         */
        public PropertyName<String> notification_content() {
            return new PropertyName<String>(this, "notification_content");
        }
        
        /**
         * published_dayのプロパティ名を返します。
         *
         * @return published_dayのプロパティ名
         */
        public PropertyName<String> published_day() {
            return new PropertyName<String>(this, "published_day");
        }
        
        /**
         * published_flgのプロパティ名を返します。
         *
         * @return published_flgのプロパティ名
         */
        public PropertyName<String> published_flg() {
            return new PropertyName<String>(this, "published_flg");
        }
    }
}