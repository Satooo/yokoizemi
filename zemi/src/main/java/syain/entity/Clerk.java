package syain.entity;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Clerkエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2016/06/17 14:33:09")
public class Clerk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** clerkIdプロパティ */
    @Id
    @Column(length = 3, nullable = false, unique = true)
    public String clerkId;

    /** nameプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String name;

    /** kananameプロパティ */
    @Column(length = 60, nullable = false, unique = false)
    public String kananame;

    /** passプロパティ */
    @Column(length = 32, nullable = false, unique = false)
    public String pass;

    /** positionプロパティ */
    @Column(length = 10, nullable = false ,unique = false)
    public int position;

    /** lendList関連プロパティ */
    //@OneToMany(mappedBy = "clerk")
    //public List<Lend> lendList;
}