package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Syain}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2016/06/03 9:32:33")
public class SyainNames {

    /**
     * syainnoのプロパティ名を返します。
     * 
     * @return syainnoのプロパティ名
     */
    public static PropertyName<String> syainno() {
        return new PropertyName<String>("syainno");
    }

    /**
     * syainnameのプロパティ名を返します。
     * 
     * @return syainnameのプロパティ名
     */
    public static PropertyName<String> syainname() {
        return new PropertyName<String>("syainname");
    }

    /**
     * passwordのプロパティ名を返します。
     * 
     * @return passwordのプロパティ名
     */
    public static PropertyName<String> password() {
        return new PropertyName<String>("password");
    }

    /**
     * retireflgのプロパティ名を返します。
     * 
     * @return retireflgのプロパティ名
     */
    public static PropertyName<String> retireflg() {
        return new PropertyName<String>("retireflg");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _SyainNames extends PropertyName<Syain> {

        /**
         * インスタンスを構築します。
         */
        public _SyainNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _SyainNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _SyainNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * syainidのプロパティ名を返します。
         *
         * @return syainidのプロパティ名
         */
        public PropertyName<String> syainno() {
            return new PropertyName<String>(this, "syainno");
        }

        /**
         * syainnameのプロパティ名を返します。
         *
         * @return syainnameのプロパティ名
         */
        public PropertyName<String> syainname() {
            return new PropertyName<String>(this, "syainname");
        }

        /**
         * passwordのプロパティ名を返します。
         *
         * @return passwordのプロパティ名
         */
        public PropertyName<String> password() {
            return new PropertyName<String>(this, "password");
        }

        /**
         * retireflgのプロパティ名を返します。
         *
         * @return retireflgのプロパティ名
         */
        public PropertyName<String> retireflg() {
            return new PropertyName<String>(this, "retireflg");
        }
    }
}