package syain.entity;


import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Clerk}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2016/06/17 14:33:16")
public class ClerkNames {

    /**
     * clerkIdのプロパティ名を返します。
     * 
     * @return clerkIdのプロパティ名
     */
    public static PropertyName<String> clerkId() {
        return new PropertyName<String>("clerkId");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * kananameのプロパティ名を返します。
     * 
     * @return kananameのプロパティ名
     */
    public static PropertyName<String> kananame() {
        return new PropertyName<String>("kananame");
    }

    /**
     * passのプロパティ名を返します。
     * 
     * @return passのプロパティ名
     */
    public static PropertyName<String> pass() {
        return new PropertyName<String>("pass");
    }

    /**
     * positionのプロパティ名を返します。
     * 
     * @return positionのプロパティ名
     */
    public static PropertyName<String> position() {
        return new PropertyName<String>("position");
    }

    /**
     * lendListのプロパティ名を返します。
     * 
     * @return lendListのプロパティ名
     */
    //public static _LendNames lendList() {
        //return new _LendNames("lendList");
    //}

    /**
     * @author S2JDBC-Gen
     */
    public static class _ClerkNames extends PropertyName<Clerk> {

        /**
         * インスタンスを構築します。
         */
        public _ClerkNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ClerkNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ClerkNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * clerkIdのプロパティ名を返します。
         *
         * @return clerkIdのプロパティ名
         */
        public PropertyName<String> clerkId() {
            return new PropertyName<String>(this, "clerkId");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * kananameのプロパティ名を返します。
         *
         * @return kananameのプロパティ名
         */
        public PropertyName<String> kananame() {
            return new PropertyName<String>(this, "kananame");
        }

        /**
         * passのプロパティ名を返します。
         *
         * @return passのプロパティ名
         */
        public PropertyName<String> pass() {
            return new PropertyName<String>(this, "pass");
        }

        /**
         * positionのプロパティ名を返します。
         *
         * @return positionのプロパティ名
         */
        public PropertyName<String> position() {
            return new PropertyName<String>(this, "position");
        }

        /**
         * lendListのプロパティ名を返します。
         * 
         * @return lendListのプロパティ名
         */
       //public _LendNames lendList() {
            //return new _LendNames(this, "lendList");
        //}
    }
}