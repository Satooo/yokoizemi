package syain.entity;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Notificationエンティティクラス
 * 
 */
@Entity
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl" }, date = "2016/05/30 11:58:24")
public class Notification implements Serializable {

	private static final long serialVersionUID = 1L;

	/** notification_codeプロパティ */
	@Id
	@GeneratedValue
	@Column(nullable = false, unique = true)
    public Integer notification_code;

	/** notification_genre_idプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String notification_genre_code;

	/** notification_titleプロパティ */
	@Column(length = 10, nullable = false, unique = false)
	public String notification_title;

	/** notification_contentプロパティ */
	@Column(length = 1, nullable = true, unique = false)
	public String notification_content;

	/** published_dayプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String published_day;

	/** publisher_flgプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String published_flg;

	/** publisher_flgで非表示にするプロパティ */
	public String getPublished_flgLabel() {
		if (published_flg.equals("0")) {
			return "公開";
		} else {
			return "非公開";
		}
	}
	
	/** notification_genre_codeで表示を変えるプロパティ*/
	public String getNotification_genre_codeLabel() {
		if (notification_genre_code.equals("01")) {
			return "事務";
		} else if (notification_genre_code.equals("02")){
			return "教務";
		} else if (notification_genre_code.equals("03")){
			return "奨学金";
		} else if (notification_genre_code.equals("09")){
			return "その他";
		} else {
		return "";
		}
	}
}