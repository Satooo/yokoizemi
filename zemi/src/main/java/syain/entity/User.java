package syain.entity;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Userエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/01/25 11:50:12")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /** useridプロパティ */
    @Id
    @Column(length = 8, nullable = false, unique = true)
    public String userid;

    /** passwordプロパティ */
    @Column(length = 8, nullable = false, unique = false)
    public String password;

    /** userNameプロパティ */
    @Column(length = 40, nullable = true, unique = false)
    public String userName;

    /** classCodeプロパティ */
    @Column(length = 6, nullable = true, unique = false)
    public String classCode;

    /** idFlgプロパティ */
    @Column(nullable = true, unique = false)
    public Boolean idFlg;

//    /** circleList関連プロパティ */
//    @OneToMany(mappedBy = "user")
//    public List<Circle> circleList;

    /** demandList関連プロパティ */
    @OneToMany(mappedBy = "user")
    public List<Demand> demandList;

//    /** responseList関連プロパティ */
//    @OneToMany(mappedBy = "user")
//    public List<Response> responseList;
//
//    /** schoolClassList関連プロパティ */
//    @OneToMany(mappedBy = "user")
//    public List<SchoolClass> schoolClassList;
//
//    /** schoolClass関連プロパティ */
//    @ManyToOne
//    @JoinColumn(name = "class_code", referencedColumnName = "class_code")
//    public SchoolClass schoolClass;
}