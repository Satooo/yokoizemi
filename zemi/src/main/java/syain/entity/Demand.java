package syain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Demandエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/01/23 10:24:38")
public class Demand implements Serializable {

    private static final long serialVersionUID = 1L;

    /** demandCodeプロパティ */
    @Id
    @Column(length = 8, nullable = false, unique = true)
    public String demandCode;

    /** demandGenreCodeプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String demandGenreCode;

    /** demandContentプロパティ */
    @Column(length = 800, nullable = false, unique = false)
    public String demandContent;

    /** studentIdプロパティ */
    @Column(length = 8, nullable = true, unique = false)
    public String studentId;

    /** demandGenre関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "demand_genre_code", referencedColumnName = "demand_genre_code")
    public DemandGenre demandGenre;
    
    /** demandDate関連プロパティ */
    @Temporal(TemporalType.DATE)
	@Column(name = "demand_date")
    public Date demandDate;
    
    /** user関連プロパティ */
    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "userId")
    public User user;
    
    
}
