package syain.entity;

import javax.annotation.Generated;
import syain.entity.CompanyNames._CompanyNames;
import syain.entity.SyainNames._SyainNames;

/**
 * 名前クラスの集約です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesAggregateModelFactoryImpl"}, date = "2016/06/03 9:32:33")
public class Names {

    /**
     * {@link Company}の名前クラスを返します。
     * 
     * @return Companyの名前クラス
     */
    public static _CompanyNames company() {
        return new _CompanyNames();
    }

    /**
     * {@link Syain}の名前クラスを返します。
     * 
     * @return Syainの名前クラス
     */
    public static _SyainNames syain() {
        return new _SyainNames();
    }
}