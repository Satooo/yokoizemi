package syain.entity;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Partエンティティクラス
 * 
 */
@Entity
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl" }, date = "2016/05/30 11:58:24")
public class Part implements Serializable {

	private static final long serialVersionUID = 1L;

	/** part_codeプロパティ */
	@Id
	@GeneratedValue //連番？
	@Column(length = 8, nullable = false, unique = true)
	public Integer part_code;//Integer型にする(連番にするため？)

	/** file_nameプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String file_name;
	
	/** conditionsプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String conditions;

	/** deadlineプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String deadline;

	/** publisher_flgプロパティ */
	@Column(length = 50, nullable = false, unique = false)
	public String published_flg;

	/** publisher_flgで非表示にするプロパティ */
	public String getPublished_flg() {
		if (published_flg.equals("0")) {
			return "公開";
		} else {
			return "非公開";
		}
	}
}