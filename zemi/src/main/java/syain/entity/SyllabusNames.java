package syain.entity;

import java.sql.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;
import syain.entity.SchoolClassNames._SchoolClassNames;

/**
 * {@link Syllabus}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/25 11:52:07")
public class SyllabusNames {

    /**
     * syllabusCodeのプロパティ名を返します。
     * 
     * @return syllabusCodeのプロパティ名
     */
    public static PropertyName<String> syllabusCode() {
        return new PropertyName<String>("syllabusCode");
    }

    /**
     * classCodeのプロパティ名を返します。
     * 
     * @return classCodeのプロパティ名
     */
    public static PropertyName<String> classCode() {
        return new PropertyName<String>("classCode");
    }

    /**
     * yearのプロパティ名を返します。
     * 
     * @return yearのプロパティ名
     */
    public static PropertyName<Date> year() {
        return new PropertyName<Date>("year");
    }

    /**
     * monthのプロパティ名を返します。
     * 
     * @return monthのプロパティ名
     */
    public static PropertyName<String> month() {
        return new PropertyName<String>("month");
    }

    /**
     * syllabusFileのプロパティ名を返します。
     * 
     * @return syllabusFileのプロパティ名
     */
    public static PropertyName<byte[]> syllabusFile() {
        return new PropertyName<byte[]>("syllabusFile");
    }

    /**
     * schoolClassのプロパティ名を返します。
     * 
     * @return schoolClassのプロパティ名
     */
    public static _SchoolClassNames schoolClass() {
        return new _SchoolClassNames("schoolClass");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _SyllabusNames extends PropertyName<Syllabus> {

        /**
         * インスタンスを構築します。
         */
        public _SyllabusNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _SyllabusNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _SyllabusNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * syllabusCodeのプロパティ名を返します。
         *
         * @return syllabusCodeのプロパティ名
         */
        public PropertyName<String> syllabusCode() {
            return new PropertyName<String>(this, "syllabusCode");
        }

        /**
         * classCodeのプロパティ名を返します。
         *
         * @return classCodeのプロパティ名
         */
        public PropertyName<String> classCode() {
            return new PropertyName<String>(this, "classCode");
        }

        /**
         * yearのプロパティ名を返します。
         *
         * @return yearのプロパティ名
         */
        public PropertyName<Date> year() {
            return new PropertyName<Date>(this, "year");
        }

        /**
         * monthのプロパティ名を返します。
         *
         * @return monthのプロパティ名
         */
        public PropertyName<String> month() {
            return new PropertyName<String>(this, "month");
        }

        /**
         * syllabusFileのプロパティ名を返します。
         *
         * @return syllabusFileのプロパティ名
         */
        public PropertyName<byte[]> syllabusFile() {
            return new PropertyName<byte[]>(this, "syllabusFile");
        }

        /**
         * schoolClassのプロパティ名を返します。
         * 
         * @return schoolClassのプロパティ名
         */
        public _SchoolClassNames schoolClass() {
            return new _SchoolClassNames(this, "schoolClass");
        }
    }
}