package syain.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Company}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2016/06/03 9:32:33")
public class CompanyNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<BigDecimal> id() {
        return new PropertyName<BigDecimal>("id");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * edateのプロパティ名を返します。
     * 
     * @return edateのプロパティ名
     */
    public static PropertyName<Date> edate() {
        return new PropertyName<Date>("edate");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _CompanyNames extends PropertyName<Company> {

        /**
         * インスタンスを構築します。
         */
        public _CompanyNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _CompanyNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _CompanyNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<BigDecimal> id() {
            return new PropertyName<BigDecimal>(this, "id");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * edateのプロパティ名を返します。
         *
         * @return edateのプロパティ名
         */
        public PropertyName<Date> edate() {
            return new PropertyName<Date>(this, "edate");
        }
    }
}