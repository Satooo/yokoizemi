package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Department}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/25 11:52:07")
public class DepartmentNames {

    /**
     * departmentCodeのプロパティ名を返します。
     * 
     * @return departmentCodeのプロパティ名
     */
    public static PropertyName<String> departmentCode() {
        return new PropertyName<String>("departmentCode");
    }

    /**
     * departmentNameのプロパティ名を返します。
     * 
     * @return departmentNameのプロパティ名
     */
    public static PropertyName<String> departmentName() {
        return new PropertyName<String>("departmentName");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _DepartmentNames extends PropertyName<Department> {

        /**
         * インスタンスを構築します。
         */
        public _DepartmentNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _DepartmentNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _DepartmentNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * departmentCodeのプロパティ名を返します。
         *
         * @return departmentCodeのプロパティ名
         */
        public PropertyName<String> departmentCode() {
            return new PropertyName<String>(this, "departmentCode");
        }

        /**
         * departmentNameのプロパティ名を返します。
         *
         * @return departmentNameのプロパティ名
         */
        public PropertyName<String> departmentName() {
            return new PropertyName<String>(this, "departmentName");
        }
    }
}