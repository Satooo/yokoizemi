package syain.entity;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.name.PropertyName;

import syain.entity.DemandNames._DemandNames;

/**
 * {@link User}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/24 16:18:14")
public class UserNames {

    /**
     * useridのプロパティ名を返します。
     * 
     * @return useridのプロパティ名
     */
    public static PropertyName<String> userid() {
        return new PropertyName<String>("userid");
    }

    /**
     * passwordのプロパティ名を返します。
     * 
     * @return passwordのプロパティ名
     */
    public static PropertyName<String> password() {
        return new PropertyName<String>("password");
    }

    /**
     * userNameのプロパティ名を返します。
     * 
     * @return userNameのプロパティ名
     */
    public static PropertyName<String> userName() {
        return new PropertyName<String>("userName");
    }

    /**
     * classCodeのプロパティ名を返します。
     * 
     * @return classCodeのプロパティ名
     */
    public static PropertyName<String> classCode() {
        return new PropertyName<String>("classCode");
    }

    /**
     * idFlgのプロパティ名を返します。
     * 
     * @return idFlgのプロパティ名
     */
    public static PropertyName<Boolean> idFlg() {
        return new PropertyName<Boolean>("idFlg");
    }


    /**
     * demandListのプロパティ名を返します。
     * 
     * @return demandListのプロパティ名
     */
    public static _DemandNames demandList() {
        return new _DemandNames("demandList");
    }



    /**
     * @author S2JDBC-Gen
     */
    public static class _UserNames extends PropertyName<User> {

        /**
         * インスタンスを構築します。
         */
        public _UserNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _UserNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _UserNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * useridのプロパティ名を返します。
         *
         * @return useridのプロパティ名
         */
        public PropertyName<String> userid() {
            return new PropertyName<String>(this, "userid");
        }

        /**
         * passwordのプロパティ名を返します。
         *
         * @return passwordのプロパティ名
         */
        public PropertyName<String> password() {
            return new PropertyName<String>(this, "password");
        }

        /**
         * userNameのプロパティ名を返します。
         *
         * @return userNameのプロパティ名
         */
        public PropertyName<String> userName() {
            return new PropertyName<String>(this, "userName");
        }

        /**
         * classCodeのプロパティ名を返します。
         *
         * @return classCodeのプロパティ名
         */
        public PropertyName<String> classCode() {
            return new PropertyName<String>(this, "classCode");
        }

        /**
         * idFlgのプロパティ名を返します。
         *
         * @return idFlgのプロパティ名
         */
        public PropertyName<Boolean> idFlg() {
            return new PropertyName<Boolean>(this, "idFlg");
        }


        /**
         * demandListのプロパティ名を返します。
         * 
         * @return demandListのプロパティ名
         */
        public _DemandNames demandList() {
            return new _DemandNames(this, "demandList");
        }

      
    }
}