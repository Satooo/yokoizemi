package syain.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Scheduleエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2017/01/25 11:50:12")
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;

    /** scheduleCodeプロパティ */
    @Id
    @Column(length = 2, nullable = false, unique = true)
    public String scheduleCode;

    /** classCodeプロパティ */
    @Column(length = 6, nullable = false, unique = false)
    public String classCode;

    /** yearプロパティ */
    @Column(nullable = false, unique = false)
    public Date year;

    /** monthプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public String month;

    /** scheduleFileプロパティ */
    @Lob
    @Column(length = 65535, nullable = false, unique = false)
    public byte[] scheduleFile;
    
    @Column
    @Temporal(TemporalType.DATE)
    public Date scheduleday;

//    /** schoolClass関連プロパティ */
//    @ManyToOne
//    @JoinColumn(name = "class_code", referencedColumnName = "class_code")
//    public SchoolClass schoolClass;
}