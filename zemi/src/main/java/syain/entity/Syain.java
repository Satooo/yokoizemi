package syain.entity;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Syainエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2016/06/16 12:23:58")
public class Syain implements Serializable {

    private static final long serialVersionUID = 1L;

    /** syainnoプロパティ */
    @Id
    @Column(length = 5, nullable = false, unique = true)
    public String syainno;

    /** syainnameプロパティ */
    @Column(length = 50, nullable = true, unique = false)
    public String syainname;

    /** passwordプロパティ */
    @Column(length = 10, nullable = true, unique = false)
    public String password;

    /** retireflgプロパティ */
    @Column(length = 1, nullable = true, unique = false)
    public String retireflg;
}