package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;
import syain.entity.DemandGenreNames._DemandGenreNames;
import syain.entity.UserNames._UserNames;

/**
 * {@link Demand}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/23 14:08:31")
public class DemandNames {

    /**
     * demandCodeのプロパティ名を返します。
     * 
     * @return demandCodeのプロパティ名
     */
    public static PropertyName<String> demandCode() {
        return new PropertyName<String>("demandCode");
    }

    /**
     * demandGenreCodeのプロパティ名を返します。
     * 
     * @return demandGenreCodeのプロパティ名
     */
    public static PropertyName<String> demandGenreCode() {
        return new PropertyName<String>("demandGenreCode");
    }

    /**
     * demandContentのプロパティ名を返します。
     * 
     * @return demandContentのプロパティ名
     */
    public static PropertyName<String> demandContent() {
        return new PropertyName<String>("demandContent");
    }

    /**
     * studentIdのプロパティ名を返します。
     * 
     * @return studentIdのプロパティ名
     */
    public static PropertyName<String> studentId() {
        return new PropertyName<String>("studentId");
    }

    /**
     * demandGenreのプロパティ名を返します。
     * 
     * @return demandGenreのプロパティ名
     */
    public static _DemandGenreNames demandGenre() {
        return new _DemandGenreNames("demandGenre");
    }

    /**
     * userのプロパティ名を返します。
     * 
     * @return userのプロパティ名
     */
    public static _UserNames user() {
        return new _UserNames("user");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _DemandNames extends PropertyName<Demand> {

        /**
         * インスタンスを構築します。
         */
        public _DemandNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _DemandNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _DemandNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * demandCodeのプロパティ名を返します。
         *
         * @return demandCodeのプロパティ名
         */
        public PropertyName<String> demandCode() {
            return new PropertyName<String>(this, "demandCode");
        }

        /**
         * demandGenreCodeのプロパティ名を返します。
         *
         * @return demandGenreCodeのプロパティ名
         */
        public PropertyName<String> demandGenreCode() {
            return new PropertyName<String>(this, "demandGenreCode");
        }

        /**
         * demandContentのプロパティ名を返します。
         *
         * @return demandContentのプロパティ名
         */
        public PropertyName<String> demandContent() {
            return new PropertyName<String>(this, "demandContent");
        }

        /**
         * studentIdのプロパティ名を返します。
         *
         * @return studentIdのプロパティ名
         */
        public PropertyName<String> studentId() {
            return new PropertyName<String>(this, "studentId");
        }

        /**
         * demandGenreのプロパティ名を返します。
         * 
         * @return demandGenreのプロパティ名
         */
        public _DemandGenreNames demandGenre() {
            return new _DemandGenreNames(this, "demandGenre");
        }

        /**
         * userのプロパティ名を返します。
         * 
         * @return userのプロパティ名
         */
        public _UserNames user() {
            return new _UserNames(this, "user");
        }
    }
}