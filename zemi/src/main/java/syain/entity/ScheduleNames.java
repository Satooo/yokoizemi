package syain.entity;

import java.sql.Date;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Schedule}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/25 11:52:07")
public class ScheduleNames {

    /**
     * scheduleCodeのプロパティ名を返します。
     * 
     * @return scheduleCodeのプロパティ名
     */
    public static PropertyName<String> scheduleCode() {
        return new PropertyName<String>("scheduleCode");
    }

    /**
     * classCodeのプロパティ名を返します。
     * 
     * @return classCodeのプロパティ名
     */
    public static PropertyName<String> classCode() {
        return new PropertyName<String>("classCode");
    }

    /**
     * yearのプロパティ名を返します。
     * 
     * @return yearのプロパティ名
     */
    public static PropertyName<Date> year() {
        return new PropertyName<Date>("year");
    }

    /**
     * monthのプロパティ名を返します。
     * 
     * @return monthのプロパティ名
     */
    public static PropertyName<String> month() {
        return new PropertyName<String>("month");
    }

    /**
     * scheduleFileのプロパティ名を返します。
     * 
     * @return scheduleFileのプロパティ名
     */
    public static PropertyName<byte[]> scheduleFile() {
        return new PropertyName<byte[]>("scheduleFile");
    }

//    /**
//     * schoolClassのプロパティ名を返します。
//     * 
//     * @return schoolClassのプロパティ名
//     */
//    public static _SchoolClassNames schoolClass() {
//        return new _SchoolClassNames("schoolClass");
//    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ScheduleNames extends PropertyName<Schedule> {

        /**
         * インスタンスを構築します。
         */
        public _ScheduleNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ScheduleNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ScheduleNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * scheduleCodeのプロパティ名を返します。
         *
         * @return scheduleCodeのプロパティ名
         */
        public PropertyName<String> scheduleCode() {
            return new PropertyName<String>(this, "scheduleCode");
        }

        /**
         * classCodeのプロパティ名を返します。
         *
         * @return classCodeのプロパティ名
         */
        public PropertyName<String> classCode() {
            return new PropertyName<String>(this, "classCode");
        }

        /**
         * yearのプロパティ名を返します。
         *
         * @return yearのプロパティ名
         */
        public PropertyName<Date> year() {
            return new PropertyName<Date>(this, "year");
        }

        /**
         * monthのプロパティ名を返します。
         *
         * @return monthのプロパティ名
         */
        public PropertyName<String> month() {
            return new PropertyName<String>(this, "month");
        }

        /**
         * scheduleFileのプロパティ名を返します。
         *
         * @return scheduleFileのプロパティ名
         */
        public PropertyName<byte[]> scheduleFile() {
            return new PropertyName<byte[]>(this, "scheduleFile");
        }

//        /**
//         * schoolClassのプロパティ名を返します。
//         * 
//         * @return schoolClassのプロパティ名
//         */
//        public _SchoolClassNames schoolClass() {
//            return new _SchoolClassNames(this, "schoolClass");
//        }
    }
}