package syain.entity;
import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/*import javax.persistence.GenerationType; */

@Entity
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl" }, date = "2016/05/30 11:58:24")


public class Circle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(length = 8, nullable = false, unique = true)
	public String circle_code;
	@Column(length = 50, nullable = false, unique = false)
	public String circle_name;
	@Column(length = 50, nullable = false, unique = false)
	public String teacher_id;
	@Column(length = 800, nullable = false, unique = false)
	public String circle_content;
	


}
