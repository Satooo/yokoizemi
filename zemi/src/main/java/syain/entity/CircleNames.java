package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Notification}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2016/06/03 9:32:33")
public class CircleNames {

    /**
     * notification_codeのプロパティ名を返します。
     * 
     * @return notification_codeのプロパティ名
     */
    public static PropertyName<String> circle_code() {
        return new PropertyName<String>("circle_code");
    }

    /**
     * notification_genre_codeのプロパティ名を返します。
     * 
     * @return notification_genre_codeのプロパティ名
     */
    public static PropertyName<String> circle_name() {
        return new PropertyName<String>("circle_name");
    }

    /**
     * notification_titleのプロパティ名を返します。
     * 
     * @return notification_titleのプロパティ名
     */
    public static PropertyName<String> teacher_id() {
        return new PropertyName<String>("teacher_id");
    }

    /**
     * notification_contentのプロパティ名を返します。
     * 
     * @return notification_contentのプロパティ名
     */
    public static PropertyName<String> circle_content() {
        return new PropertyName<String>("circle_content");
    }
    
   

    /**
     * @author S2JDBC-Gen
     */
    public static class _CircleNames extends PropertyName<Circle> {

        /**
         * インスタンスを構築します。
         */
        public _CircleNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _CircleNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _CircleNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * notification_codeのプロパティ名を返します。
         *
         * @return notification_codeのプロパティ名
         */
        public PropertyName<String> circle_code() {
            return new PropertyName<String>(this, "circle_code");
        }

        /**
         * notification_genre_idのプロパティ名を返します。
         *
         * @return notification_genre_idのプロパティ名
         */
        public PropertyName<String> circle_name() {
            return new PropertyName<String>(this, "circle_name");
        }

        /**
         * notification_titleのプロパティ名を返します。
         *
         * @return notification_titleのプロパティ名
         */
        public PropertyName<String> teacher_id() {
            return new PropertyName<String>(this, "teacher_id");
        }

        /**
         * notification_contentのプロパティ名を返します。
         *
         * @return notification_contentのプロパティ名
         */
        public PropertyName<String> notification_content() {
            return new PropertyName<String>(this, "circle_content");
        }
        
        /**
         * published_dayのプロパティ名を返します。
         *
         * @return published_dayのプロパティ名
         */

    }
}