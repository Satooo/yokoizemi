package syain.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;
import syain.entity.DemandNames._DemandNames;

/**
 * {@link DemandGenre}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2017/01/24 16:18:14")
public class DemandGenreNames {

    /**
     * demandGenreCodeのプロパティ名を返します。
     * 
     * @return demandGenreCodeのプロパティ名
     */
    public static PropertyName<String> demandGenreCode() {
        return new PropertyName<String>("demandGenreCode");
    }

    /**
     * demandGenreNameのプロパティ名を返します。
     * 
     * @return demandGenreNameのプロパティ名
     */
    public static PropertyName<String> demandGenreName() {
        return new PropertyName<String>("demandGenreName");
    }

    /**
     * demandListのプロパティ名を返します。
     * 
     * @return demandListのプロパティ名
     */
    public static _DemandNames demandList() {
        return new _DemandNames("demandList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _DemandGenreNames extends PropertyName<DemandGenre> {

        /**
         * インスタンスを構築します。
         */
        public _DemandGenreNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _DemandGenreNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _DemandGenreNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * demandGenreCodeのプロパティ名を返します。
         *
         * @return demandGenreCodeのプロパティ名
         */
        public PropertyName<String> demandGenreCode() {
            return new PropertyName<String>(this, "demandGenreCode");
        }

        /**
         * demandGenreNameのプロパティ名を返します。
         *
         * @return demandGenreNameのプロパティ名
         */
        public PropertyName<String> demandGenreName() {
            return new PropertyName<String>(this, "demandGenreName");
        }

        /**
         * demandListのプロパティ名を返します。
         * 
         * @return demandListのプロパティ名
         */
        public _DemandNames demandList() {
            return new _DemandNames(this, "demandList");
        }
    }
}