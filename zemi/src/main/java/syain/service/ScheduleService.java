package syain.service;

import static org.seasar.extension.jdbc.operation.Operations.asc;
import static syain.entity.ScheduleNames.scheduleCode;

import java.util.List;

import javax.annotation.Generated;

import syain.entity.Schedule;

/**
 * {@link Schedule}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/01/23 14:08:33")
public class ScheduleService extends AbstractService<Schedule> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param scheduleCode
	 *            識別子
	 * @return エンティティ
	 */
	public Schedule findById(String scheduleCode) {
		return select().id(scheduleCode).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Schedule> findAllOrderById() {
		return select().orderBy(asc(scheduleCode())).getResultList();
	}

	public class SelectAllParam {
		public String name1;
		public String name2;
	}

	// 受け取った学科,学年を元に、先月 来月の講義予定表を検索、リストとして受け取る
	public List<Schedule> scheduleSearch(String scheduleselectcourse, String scheduleselectschoolyear) {
		SelectAllParam param = new SelectAllParam();
		param.name1 = new String(scheduleselectcourse);
		param.name2 = new String(scheduleselectschoolyear);

		return jdbcManager
				.selectBySqlFile(Schedule.class, "data/Schedule.sql", param)
				.getResultList();
	}

}