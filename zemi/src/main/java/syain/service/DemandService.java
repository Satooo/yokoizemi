package syain.service;

import static org.seasar.extension.jdbc.operation.Operations.*;
import static syain.entity.DemandNames.*;

import java.util.List;

import javax.annotation.Generated;

import org.seasar.framework.beans.util.Beans;

import syain.dto.DemandDto;
import syain.entity.Demand;
import syain.entity.DemandGenre;

/**
 * {@link Demand}のサービスクラスです。
 *
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/01/22 20:28:13")
public class DemandService extends AbstractService<Demand> {

	/**
	 * 識別子でエンティティを検索します。
	 *
	 * @param demandCode
	 *            識別子
	 * @return エンティティ
	 */
	public Demand findById(String demandCode) {
		return select().id(demandCode).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 *
	 * @return エンティティのリスト
	 */
	public List<Demand> findAllOrderById() {
		return select().orderBy(asc(demandCode())).getResultList();
	}

	// 最も大きなコード取得
	public String maxCode() {
		int result = 8;
		Demand demand = 
				 jdbcManager
			        .selectBySql(
			            Demand.class, 
			            "select MAX(demand_code)demand_code from demand")
				.getSingleResult();
		try {
			String a = demand.demandCode;
			result = Integer.parseInt(a);
			result+=1;
		} catch (Exception e) {
			result = 500;
		}

		return String.valueOf(result);
	}

	/*
	 * 受け取ったdemandgenreからジャンルコードを検索するメソッド
	 * @return String
	 */
	
	public String genreSearch(String demand_genre) {
		
		DemandGenre genre = 
			    jdbcManager
			        .from(DemandGenre.class)
			        .where("demand_genre_name = ?", demand_genre)
			        .getSingleResult();
		
		return genre.demandGenreCode;
	}

	//挿入
	public int insert(DemandDto demandDto) {
		Demand demand = Beans.createAndCopy(Demand.class, demandDto).dateConverter("yyyy/MM/dd").execute();
		return jdbcManager.insert(demand).execute();
	}
}