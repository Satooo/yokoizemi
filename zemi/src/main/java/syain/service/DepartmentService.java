package syain.service;

import static org.seasar.extension.jdbc.operation.Operations.asc;
import static syain.entity.DepartmentNames.departmentCode;

import java.util.List;

import javax.annotation.Generated;

import syain.entity.Department;

/**
 * {@link Department}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/01/23 14:08:33")
public class DepartmentService extends AbstractService<Department> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param departmentCode
	 *            識別子
	 * @return エンティティ
	 */
	public Department findById(String departmentCode) {
		return select().id(departmentCode).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Department> findAllOrderById() {
		return select().orderBy(asc(departmentCode())).getResultList();
	}

	/**
	 * 学科を引数として受け取り、学科コードを検索します
	 * 
	 * @return 学科コード
	 */
	public String departmentSearch(String selectcourse) {

		Department department = 
				jdbcManager
				.from(Department.class)
				.where("department_name = ?",selectcourse )
				.getSingleResult();

		return department.departmentCode;
	}
	

}