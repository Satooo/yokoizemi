package syain.service;

import java.math.BigDecimal;
import java.util.List;
import javax.annotation.Generated;
import syain.entity.Company;

import static org.seasar.extension.jdbc.operation.Operations.*;
import static syain.entity.CompanyNames.*;

/**
 * {@link Company}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/05/30 11:58:28")
public class CompanyService extends AbstractService<Company> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public Company findById(BigDecimal id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Company> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}