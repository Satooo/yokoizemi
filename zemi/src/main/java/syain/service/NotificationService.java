package syain.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;

import org.seasar.framework.beans.util.Beans;

import syain.dto.NotificationDto;
import syain.entity.Notification;

import static org.seasar.extension.jdbc.operation.Operations.*;
import static syain.entity.NotificationNames.*;

/**
 * {@link Notification}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/05/30 11:09:32")
public class NotificationService extends AbstractService<Notification> {
	
	Date date = new Date();
	Calendar cal = Calendar.getInstance();
    DateFormat df = new SimpleDateFormat("yyyy-MM");
    String day = df.format(date);
    /**
     * 識別子でエンティティを検索します。
     * 
     * @param notification_code
     *            識別子
     * @return エンティティ
     */
    public Notification findById(String notification_code) {
        return select().id(notification_code).getSingleResult();
    }
    
    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Notification> findAllOrderById(String notification_code) {
        return select().where(eq(published_flg(),"0")).orderBy(desc(notification_code())).getResultList();
    }
    
    public List<Notification> findAllOrderByIdPast1(String published_flg,String published_day) {
    	cal.add(Calendar.MONTH, -1);
        return select().where(eq(published_flg(),"0"),and(starts(published_day(),df.format(cal.getTime())))).orderBy(desc(notification_code())).getResultList();
    }
    
    public List<Notification> findAllOrderByIdPast2(String published_flg,String published_day) {
    	cal.add(Calendar.MONTH, -2);
        return select().where(eq(published_flg(),"0"),and(starts(published_day(),df.format(cal.getTime())))).orderBy(desc(notification_code())).getResultList();
    }
    
    public List<Notification> findAllOrderByIdPast3(String published_flg,String published_day) {
    	cal.add(Calendar.MONTH, -3);
        return select().where(eq(published_flg(),"0"),and(starts(published_day(),df.format(cal.getTime())))).orderBy(desc(notification_code())).getResultList();
    }
    
    public List<Notification> findAllOrderByIdPast4(String published_flg,String published_day) {
    	cal.add(Calendar.MONTH, -4);
        return select().where(eq(published_flg(),"0"),and(starts(published_day(),df.format(cal.getTime())))).orderBy(desc(notification_code())).getResultList();
    }
    
    public List<Notification> findAllOrderByIdPast5(String published_flg,String published_day) {
    	cal.add(Calendar.MONTH, -5);
        return select().where(eq(published_flg(),"0"),and(starts(published_day(),df.format(cal.getTime())))).orderBy(desc(notification_code())).getResultList();
    }

    public List<Notification> findAllOrderByIdAdmin() {
        return select().orderBy(desc(notification_code())).getResultList();
    }
    
    public int insert(NotificationDto notificationDto){
    	Notification notification = Beans.createAndCopy(Notification.class, notificationDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.insert(notification).execute();
    }
    
    public int update(NotificationDto notificationDto){
    	Notification notification = Beans.createAndCopy(Notification.class, notificationDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.update(notification).execute();
    }
    
    public int delete(NotificationDto notificationDto){
    	Notification notification = Beans.createAndCopy(Notification.class, notificationDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.delete(notification).execute();
    }
}