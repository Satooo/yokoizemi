package syain.service;

import syain.entity.Syain;
import java.util.List;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.where.SimpleWhere;

import static syain.entity.SyainNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Syain}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/06/16 12:24:07")
public class SyainService extends AbstractService<Syain> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param syainno
     *            識別子
     * @return エンティティ
     */
    public boolean findByIdPwd(String userId,String password) {
    	boolean flg=false;
    	Syain results =jdbcManager.from(Syain.class) .where(
                new SimpleWhere().starts("syainno", userId).ends("password", password)).getSingleResult();
    	try {
    		if(results.syainno.equals(userId)) {
    			flg = true;
    		}
    	   } catch (NullPointerException e) {
    		   e.getMessage();
    	   }
		return flg;
    }

 

	/**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Syain> findAllOrderById() {
        return select().orderBy(asc(syainno())).getResultList();
    }
}