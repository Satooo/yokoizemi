package syain.service;

import java.util.List;

import javax.annotation.Generated;

import org.seasar.framework.beans.util.Beans;

import syain.dto.PartDto;
import syain.entity.Part;

import static org.seasar.extension.jdbc.operation.Operations.*;
import static syain.entity.PartNames.*;

/**
 * {@link Part}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/05/30 11:09:32")
public class PartService extends AbstractService<Part> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param part_code
     *            識別子
     * @return エンティティ
     */
    public Part findById(String part_code) {
        return select().id(part_code).getSingleResult();
    }
    
    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Part> findAllOrderById() {
        return select().orderBy(desc(deadline())).getResultList();
    }

	public int insert(PartDto partDto){
    	Part part = Beans.createAndCopy(Part.class, partDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.insert(part).execute();
    }
	
	public int update(PartDto partDto){
    	Part part = Beans.createAndCopy(Part.class, partDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.update(part).execute();
    }
	
	public int delete(PartDto partDto){
    	Part part = Beans.createAndCopy(Part.class, partDto)
    			.dateConverter("yyyy/MM/dd").execute();
    	return jdbcManager.delete(part).execute();
    }
}