package syain.service;

import static org.seasar.extension.jdbc.operation.Operations.asc;
import static syain.entity.SyllabusNames.syllabusCode;

import java.util.List;

import javax.annotation.Generated;

import syain.entity.Schedule;
import syain.entity.Syllabus;

/**
 * {@link Syllabus}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/01/23 14:08:33")
public class SyllabusService extends AbstractService<Syllabus> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param syllabusCode
	 *            識別子
	 * @return エンティティ
	 */
	public Syllabus findById(String syllabusCode) {
		return select().id(syllabusCode).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<Syllabus> findAllOrderById() {
		return select().orderBy(asc(syllabusCode())).getResultList();
	}

	// 受け取った学科から、1年から4年のシラバスを検索、リストとして受け取る
	// 今年の4月一日から3月31日まで
	public List<Syllabus> syllabusSearch(String department_code) {
//		return jdbcManager
//				.selectBySqlFile(Schedule.class, "data/Schedule.sql", param)
//				.getResultList();
		
//		//先月、今月、来月を取得するsql文
//		select * 
//		from syllabus
//		WHERE syllabusday >= DATE_ADD(NOW(), INTERVAL -1 MONTH)
//		or  DATE_ADD(NOW(), INTERVAL 1 MONTH);		
		
    	return jdbcManager
        .selectBySql(
        		Syllabus.class, 
            "select *, CAST(syllabus_file AS CHAR(10000) CHARACTER SET utf8) from syllabus")
	.getResultList();
		
	}
}