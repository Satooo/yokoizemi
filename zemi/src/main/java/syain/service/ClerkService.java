package syain.service;

import syain.dto.ClerkDto;
import syain.entity.Clerk;

import java.util.List;
import javax.annotation.Generated;

import org.seasar.framework.beans.util.Beans;

import static syain.entity.ClerkNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Clerk}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/06/17 14:33:23")
public class ClerkService extends AbstractService<Clerk> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param clerkId
     *            識別子
     * @return エンティティ
     */
    public Clerk findById(String clerkId) {
        return select().id(clerkId).getSingleResult();
    }
    
    public List<Clerk> getClerkList() {
    	List<Clerk> results = jdbcManager.from(Clerk.class).getResultList();
		return results;
    }
    
    public int updatePosition(ClerkDto clerkDto) {
    	Clerk clerk = jdbcManager.from(Clerk.class).id(clerkDto.getClerkId()).getSingleResult();
		clerk.position = clerkDto.getPosition();
		return jdbcManager.update(clerk).execute();
    }

    public String getMaxclerkId() {
    	String result = jdbcManager.selectBySql(String.class,
    			"SELECT MAX(CLERK_ID)+1  FROM clerk ").getSingleResult();
        return result;
    }
    
    public boolean findByIdPwd(String userId,String password) {
    	boolean flg=false;
    	Clerk results = jdbcManager.selectBySql(Clerk.class,
			"SELECT *"
				+ " FROM clerk where clerk_id = ? AND pass = ?",
		userId,password).getSingleResult();
    	try {
    		if(results.clerkId.equals(userId)) {
    			flg = true;
    		}
    	   } catch (NullPointerException e) {
    		   e.getMessage();
    	   }
		return flg;
    }
    
    public int insert(ClerkDto clerkDto) {
		Clerk clerk = Beans.createAndCopy(Clerk.class,clerkDto).execute();
		return jdbcManager.insert(clerk).execute();
	}
    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Clerk> findAllOrderById() {
        return select().orderBy(asc(clerkId())).getResultList();
    }
}