package syain.service;

import static org.seasar.extension.jdbc.operation.Operations.asc;
import static syain.entity.UserNames.userid;

import java.util.List;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.where.SimpleWhere;
import org.seasar.framework.beans.util.Beans;

import syain.dto.UserDto;
import syain.entity.User;

/**
 * {@link User}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.46",
		"org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2017/01/25 11:52:00")
public class UserService extends AbstractService<User> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param userid
	 *            識別子
	 * @return エンティティ
	 */
	public User findById(String userid) {
		return select().id(userid).getSingleResult();
	}
	
	public List<User> getUserList() {
    	List<User> results = jdbcManager.from(User.class).getResultList();
		return results;
    }
    
    public int updateUser(UserDto userDto) {
    	User user = jdbcManager.from(User.class).id(userDto.getUserid()).getSingleResult();
    	//user.position = userDto.getPosition();
		return jdbcManager.update(user).execute();
    }

    public String getMaxuserId() {
    	String result = jdbcManager.selectBySql(String.class,
    			"SELECT MAX(CLERK_ID)+1  FROM user ").getSingleResult();
        return result;
    }
    
    public boolean findByIdPwd(String userid,String password) {
    	boolean flg=false;
    	User results = jdbcManager.selectBySql(User.class,
			"SELECT *"
				+ " FROM user where Userid = ? AND password = ?",
		userid,password).getSingleResult();
    	try {
    		if(results.userid.equals(userid)) {
    			flg = true;
    		}
    	   } catch (NullPointerException e) {
    		   e.getMessage();
    	   }
		return flg;
    }
    
    public int insert(UserDto userDto) {
    	User user = Beans.createAndCopy(User.class,userDto).execute();
		return jdbcManager.insert(user).execute();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<User> findAllOrderById() {
		return select().orderBy(asc(userid())).getResultList();
	}
	
	
	/**
	 * 学科コードを引数として受け取り、教員のエンティティを検索します
	 * 
	 * @return userエンティティのリスト
	 */
	public List<User> teacherSearch(String department_code) {
		return  jdbcManager
				.from(User.class)
                .where(new SimpleWhere().starts("classCode",department_code).eq("idflg", 1))
				.getResultList();
	}
	
	//担当サークルを検索
	public String circleGet(String userid) {
			String result =  jdbcManager
			        .selectBySql(
			            String.class, 
			            "select circle_name from circle where teacher_id = ?" , userid)
				.getSingleResult();
			if(result == null){
				result = "無所属";
			}
			
		return result;
	}
	
	
}