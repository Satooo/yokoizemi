package syain.service;

import java.util.List;

import syain.entity.Circle;
import syain.entity.Notification;

import java.math.BigDecimal;
import javax.annotation.Generated;

import static org.seasar.extension.jdbc.operation.Operations.*;
import static syain.entity.CompanyNames.*;
import static syain.entity.NotificationNames.published_day;


@Generated(value = {"S2JDBC-Gen 2.4.46", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2016/05/30 11:58:28")
public class CircleService extends AbstractService<Circle> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public Circle findById(BigDecimal circle_code) {
        return select().id(circle_code).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     
    public List<Circle> findAllOrderById() {
        return select().orderBy(desc(circle_code())).getResultList();
    }*/
    
    public List<Circle> getCircleList() {
    	List<Circle> results = jdbcManager.from(Circle.class).getResultList();
		return results;
    }


}





