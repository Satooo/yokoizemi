package syain.dto;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class UserDataDto implements Serializable {
	private static final long serialVersionUID = 1L;

	public String user_id;
	public String password;
	public String user_name;
	public String class_code;
	public String id_flg;
	
	
	public String getUser_id() {
		return user_id;
	}
	
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public String getClass_code() {
		return class_code;
	}

	public void setClass_code(String class_code) {
		this.class_code = class_code;
	}
	
	public String getUserId() {
		return user_id;
	}

	public void setUserId(String user_id) {
		this.user_id = user_id;
	}
	
	public String getId_flg() {
		return id_flg;
	}

	public void setId_flg(String id_flg) {
		this.id_flg = id_flg;
	}
}