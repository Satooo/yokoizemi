package syain.dto;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class PartDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String part_code;
	private String file_name;
	private String conditions;
	private String deadline;
	private String published_flg;
	
	
	public void reset() {
		this.setPart_code("");
		this.setFile_name("");
		this.setConditions("");
		this.setDeadline("");
		this.setPublished_flg("");
	}


	public String getPart_code() {
		return part_code;
	}


	public void setPart_code(String part_code) {
		this.part_code = part_code;
	}


	public String getFile_name() {
		return file_name;
	}


	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}


	public String getPublished_flg() {
		return published_flg;
	}


	public void setPublished_flg(String published_flg) {
		this.published_flg = published_flg;
	}


	public String getConditions() {
		return conditions;
	}


	public void setConditions(String conditions) {
		this.conditions = conditions;
	}


	public String getDeadline() {
		return deadline;
	}


	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}


}
