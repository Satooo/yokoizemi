package syain.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class AdministratorDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String administrator_code;
	private String administrator_name;
	private String password;
	
	public void reset() {
		this.setAdministrator_code("");
		this.setAdministrator_name("");
		this.setPassword("");
	}


	public String getAdministrator_code() {
		return administrator_code;
	}


	public void setAdministrator_code(String administrator_code) {
		this.administrator_code = administrator_code;
	}


	public String getAdministrator_name() {
		return administrator_name;
	}


	public void setAdministrator_name(String administrator_name) {
		this.administrator_name = administrator_name;
	}

	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
}
