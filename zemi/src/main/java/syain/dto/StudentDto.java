package syain.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class StudentDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String student_number;
	private String attendence_number;
	private String user_name;
	private String birthday;
	private String password;
	private String circle_code;
	private String class_code;
	
	public void reset() {
		this.setStudent_number("");
		this.setAttendence_number("");
		this.setUser_name("");
		this.setBirthday("");
		this.setPassword("");
		this.setCircle_code("");
		this.setClass_code("");
	}


	public String getStudent_number() {
		return student_number;
	}


	public void setStudent_number(String student_number) {
		this.student_number = student_number;
	}


	public String getAttendence_number() {
		return attendence_number;
	}


	public void setAttendence_number(String attendence_number) {
		this.attendence_number = attendence_number;
	}


	public String getUser_name() {
		return user_name;
	}


	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}


	public String getBirthday() {
		return birthday;
	}


	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getCircle_code() {
		return circle_code;
	}


	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}


	public String getClass_code() {
		return class_code;
	}


	public void setClass_code(String class_code) {
		this.class_code = class_code;
	}
}
