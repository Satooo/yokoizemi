package syain.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class CircleDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String circle_code;
	private String circle_name;
	private String teacher_id;
	private String circle_content;

	
	public void reset() {
		this.setCircle_code("");
		this.setCircle_name("");
		this.setTeacher_id("");
		this.setCircle_content("");

	}


	public String getCircle_code() {
		return circle_code;
	}


	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}


	public String getCircle_name() {
		return circle_name;
	}


	public void setCircle_name(String circle_name) {
		this.circle_name = circle_name;
	}


	public String getTeacher_id() {
		return teacher_id;
	}


	public void setTeacher_id(String teacher_id) {
		this.teacher_id = teacher_id;
	}


	public String getCircle_content() {
		return circle_content;
	}


	public void setCircle_content(String circle_content) {
		this.circle_content = circle_content;
	}



}
