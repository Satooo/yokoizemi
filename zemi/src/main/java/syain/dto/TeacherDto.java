package syain.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class TeacherDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String ｔeacher_number;
	private String ｔeacher_name;
	private String password;
	private String circle_code;
	private String class_code;
	
	public void reset() {
		this.setTeacher_number("");
		this.setTeacher_name("");
		this.setPassword("");
		this.setCircle_code("");
		this.setClass_code("");
	}


	public String getStudent_number() {
		return ｔeacher_number;
	}


	public void setTeacher_number(String ｔeacher_number) {
		this.ｔeacher_number = ｔeacher_number;
	}


	public String getTeacher_name() {
		return ｔeacher_name;
	}


	public void setTeacher_name(String ｔeacher_name) {
		this.ｔeacher_name = ｔeacher_name;
	}

	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getCircle_code() {
		return circle_code;
	}


	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}


	public String getClass_code() {
		return class_code;
	}


	public void setClass_code(String class_code) {
		this.class_code = class_code;
	}
}
