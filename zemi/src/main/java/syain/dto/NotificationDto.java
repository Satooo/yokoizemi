package syain.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class NotificationDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String notification_code;
	private String notification_genre_code;
	private String notification_title;
	private String notification_content;
	private String published_day;
	private String published_flg;
	
	
	public void reset() {
		this.setNotification_code("");
		this.setNotification_genre_code("");
		this.setNotification_title("");
		this.setNotification_content("");
		this.setPublished_day("");
		this.setPublished_flg("");
	}


	public String getNotification_code() {
		return notification_code;
	}


	public void setNotification_code(String notification_code) {
		this.notification_code = notification_code;
	}


	public String getNotification_genre_code() {
		return notification_genre_code;
	}


	public void setNotification_genre_code(String notification_genre_code) {
		this.notification_genre_code = notification_genre_code;
	}


	public String getNotification_title() {
		return notification_title;
	}


	public void setNotification_title(String notification_title) {
		this.notification_title = notification_title;
	}


	public String getNotification_content() {
		return notification_content;
	}


	public void setNotification_content(String notification_content) {
		this.notification_content = notification_content;
	}


	public String getPublished_day() {
		return published_day;
	}


	public void setPublished_day(String published_day) {
		this.published_day = published_day;
	}


	public String getPublished_flg() {
		return published_flg;
	}


	public void setPublished_flg(String published_flg) {
		this.published_flg = published_flg;
	}
}
