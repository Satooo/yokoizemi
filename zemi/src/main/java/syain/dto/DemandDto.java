package syain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

import syain.entity.DemandGenre;
import syain.entity.User;

/**
 * Demandエンティティクラス
 * 
 */
@Component(instance = InstanceType.SESSION)
public class DemandDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** demandCodeプロパティ */
	@Id
	@Column(length = 8, nullable = false, unique = true)
	private String demand_code;

	/** demandGenreCodeプロパティ */
	@Column(length = 2, nullable = false, unique = false)
	private String demand_genre_code;

	/** demandContentプロパティ */
	@Column(length = 800, nullable = false, unique = false)
	private String demand_content;

	/** studentIdプロパティ */
	@Column(length = 8, nullable = true, unique = false)
	private String student_id;

	/** demandGenre関連プロパティ */
	@ManyToOne
	@JoinColumn(name = "demand_genre_code", referencedColumnName = "demand_genre_code")
	private DemandGenre demand_genre;

	 /** demandDate関連プロパティ */
    @Temporal(TemporalType.DATE)
	@Column(name = "demand_date")
    private Date demandDate;
	
	/** user関連プロパティ */
	@ManyToOne
	@JoinColumn(name = "student_id", referencedColumnName = "user_id")
	private User user;
	

	public String getDemandCode() {
		return demand_code;
	}

	public void setDemandCode(String demand_code) {
		this.demand_code = demand_code;
	}

	public String getDemandGenreCode() {
		return demand_genre_code;
	}

	public void setDemandGenreCode(String demand_genre_code) {
		this.demand_genre_code = demand_genre_code;
	}

	public String getDemandContent() {
		return demand_content;
	}

	public void setDemandContent(String demand_content) {
		this.demand_content = demand_content;

	}

	public String getStudentId() {
		return student_id;
	}

	public void setStudentId(String student_id) {
		this.student_id = student_id;
	}

	public DemandGenre getDemandGenre() {
		return demand_genre;
	}

	public void setDemandGenre(DemandGenre demand_genre) {
		this.demand_genre = demand_genre;
	}

	public Date getDemandDate() {
		return demandDate;
	}

	public void setDemandDate(Date demandDate) {
		this.demandDate = demandDate;
	}
}