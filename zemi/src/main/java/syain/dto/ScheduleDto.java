package syain.dto;

import java.io.Serializable;
import java.sql.Date;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class ScheduleDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public String scheduleCode;

	public String classCode;

	public Date year;

	public String month;

	public byte[] scheduleFile;
	

	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public byte[] getScheduleFile() {
		return scheduleFile;
	}

	public void setScheduleFile(byte[] scheduleFile) {
		this.scheduleFile = scheduleFile;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

