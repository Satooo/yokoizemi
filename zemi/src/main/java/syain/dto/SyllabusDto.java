package syain.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class SyllabusDto {

	public static final String TABLE = "syllabus";
	@Id
	@GeneratedValue
	@Column(name = "syllabus_code")
	private String syllabusCode;
	@Column(name = "class_code")
	private String classCode;
	@Temporal(TemporalType.DATE)
	private Date year;
	private String month;
	@Column(name = "syllabus_file")
	public byte[] syllabusFile;

	public String getSyllabusCode() {
		return syllabusCode;
	}

	public void setSyllabusCode(String syllabusCode) {
		this.syllabusCode = syllabusCode;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public byte[] getSyllabusFile() {
		return syllabusFile;
	}

	public void setSyllabusFile(byte[] syllabusFile) {
		this.syllabusFile = syllabusFile;
	}

}
