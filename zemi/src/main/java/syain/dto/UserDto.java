package syain.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class UserDto {

	public static final String TABLE = "user";
	@Id
	@GeneratedValue
	public String userid;
	public String password;
	@Column(name = "user_name")
	public String userName;
	@Column(name = "class_code")
	public String classCode;
	@Column(name = "id_flg")
	public Boolean idFlg;
	public String circle;

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public Boolean getIdFlg() {
		return idFlg;
	}

	public void setIdFlg(Boolean idFlg) {
		this.idFlg = idFlg;
	}

}
