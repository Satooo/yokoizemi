package syain.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.dto.PartDto;
import syain.entity.Part;
import syain.form.PartForm;
import syain.service.PartService;

public class PartAction {
	
	public List<Part> partList;
	public Part part;
	
	Date date = new Date();
	
	 SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	
	@Resource
	protected PartService partService;
	
	@Resource
	@ActionForm
	protected PartForm partForm;

	@Resource
	protected PartDto partDto;
	
	@Execute(validator = false)
   	public String part(){
		partList = partService.findAllOrderById();
   	return "part.jsp";
    }
	
	/* バイト管理者用 */
    @Execute(validator = false)
    	public String part_admin() {
		partList = partService.findAllOrderById();
    	return "part_admin.jsp";
    }
    
    /*バイト新規作成*/
	@Execute(validator = false)
   	public String part_ins(){
   	return "part_ins.jsp";
    }
	
    @Execute(validator =true,input="part_ins.jsp")
	public String part_ins_fin() {
		partList = partService.findAllOrderById();
		partDto.setPart_code(partForm.part_code);
		partDto.setFile_name(partForm.file_name);
		partDto.setConditions(partForm.conditions);
		partDto.setDeadline(partForm.deadline);
		partDto.setPublished_flg("0");
		partService.insert(partDto);
		partList = partService.findAllOrderById();
	return "part_admin.jsp";
    }
	
	/*バイト更新*/
	@Execute(validator = false , input="part_admin.jsp")
   	public String part_upd(){
		part = partService.findById(partForm.part_code);
		partDto.setPart_code(partForm.part_code);
		partDto.setFile_name(partForm.file_name);
		partDto.setConditions(partForm.conditions);
		partDto.setDeadline(partForm.deadline);
		partDto.setPublished_flg("0");
   	return "part_upd.jsp";
    }
	
	
	@Execute(validator = true,input="part_upd.jsp")
	public String part_upd_fin() {
		partList = partService.findAllOrderById();
		partDto.setPart_code(partForm.part_code);
		partDto.setFile_name(partForm.file_name);
		partDto.setConditions(partForm.conditions);
		partDto.setDeadline(partForm.deadline);
		partDto.setPublished_flg("0");
		partService.update(partDto);
		partList = partService.findAllOrderById();
	return "part_admin.jsp";
    }
	
	/* バイト削除 */
	@Execute(validator = false,input="part_admin.jsp")
   	public String part_del(){
		partDto.setPart_code(partForm.part_code);
		partDto.setFile_name(partForm.file_name);
		partDto.setConditions(partForm.conditions);
		partDto.setDeadline(partForm.deadline);
		partDto.setPublished_flg("0");
   	return "part_del.jsp";
    }
	
	@Execute(validator = false,input="part_upd.jsp")
	public String part_del_fin() {
		partList = partService.findAllOrderById();
		partDto.setPart_code(partForm.part_code);
		partDto.setFile_name(partForm.file_name);
		partDto.setConditions(partForm.conditions);
		partDto.setDeadline(partForm.deadline);
		partDto.setPublished_flg("0");
		partService.delete(partDto);
		partList = partService.findAllOrderById();
	return "part_admin.jsp";
}
}
