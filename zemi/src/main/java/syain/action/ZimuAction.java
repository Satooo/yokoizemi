package syain.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.dto.PartDto;
import syain.entity.Part;
import syain.form.PartForm;
import syain.service.PartService;

public class ZimuAction extends NotificationAction {
	
	public List<Part> partList;
	public Part part;
	
	Date date = new Date();
	
	 SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	
	@Resource
	protected PartService partService;
	
	@Resource
	@ActionForm
	protected PartForm partForm;

	@Resource
	protected PartDto partDto;

	
	/* 事務トップ */
    @Execute(validator = false)
    	public String zimu() {
    	return "zimu.jsp";
    }
    
    /* 事務バイト情報 */
    
    @Execute(validator = false)
    	public String jobInfo() {
    	partList = partService.findAllOrderById();
    	return "/part/part.jsp";
    }
    
    /* 事務商品情報 */
    
    @Execute(validator = false)
    	public String productInfo() {
    	return "/zimu/productInfo.jsp";
    }
    
    /* 奨学金情報 */
    
    @Execute(validator = false)
    	public String shogakuInfo() {
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
    	return "/zimu/shogakuInfo.jsp";
    }
    
}
