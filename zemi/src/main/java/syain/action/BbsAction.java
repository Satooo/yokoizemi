package syain.action;

import org.seasar.struts.annotation.Execute;

public class BbsAction {

	
	/* 掲示板トップ */
    @Execute(validator = false)
    	public String Bbs() {
    	return "bbs.jsp";
    }
    
	
	/* 掲示板 */
    @Execute(validator = false)
    	public String bbs() {
    	return "/bbs/bbs.jsp";
    }
    
}
