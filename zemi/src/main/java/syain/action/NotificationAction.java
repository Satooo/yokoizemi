package syain.action;

import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.dto.NotificationDto;
import syain.entity.Notification;
import syain.form.NotificationForm;
import syain.service.NotificationService;

public class NotificationAction {
	
	public List<Notification> notificationList;
	public List<Notification> notificationSideList;
	public Notification notification;
	
	Date date = new Date();
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	 
	@Resource
	protected NotificationService notificationService;

	@Resource
	@ActionForm
	protected NotificationForm notificationForm;

	@Resource
	protected NotificationDto notificationDto;
	
	@Execute(validator = false)
   	public String notification(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
   	return "notification.jsp";
    }
	
	@Execute(validator = false)
   	public String notification1(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdPast1(notificationForm.published_flg,notificationForm.published_day);
   	return "notification.jsp";
    }
	
	@Execute(validator = false)
   	public String notification2(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdPast2(notificationForm.published_flg,notificationForm.published_day);
   	return "notification.jsp";
    }
	
	@Execute(validator = false)
   	public String notification3(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdPast3(notificationForm.published_flg,notificationForm.published_day);
   	return "notification.jsp";
    }
	
	@Execute(validator = false)
   	public String notification4(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdPast4(notificationForm.published_flg,notificationForm.published_day);
   	return "notification.jsp";
    }
	
	@Execute(validator = false)
   	public String notification5(){
		notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdPast5(notificationForm.published_flg,notificationForm.published_day);
   	return "notification.jsp";
    }
	
	/* お知らせ管理者用 */
    @Execute(validator = false)
    	public String notification_admin() {
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
    	return "notification_admin.jsp";
    }
    
    /*お知らせ新規登録*/
    @Execute(validator = false)
   	public String notification_ins(){
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
    	notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
   	return "notification_ins.jsp";
    }
    
    @Execute(validator = true,input="notification_ins.jsp")
	public String notification_ins_fin() {
		notificationDto.setNotification_code(notificationForm.notification_code);
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg("0");
		notificationService.insert(notificationDto);
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
	return "notification_admin.jsp";
    }
    
	/*お知らせ更新 */
	@Execute(validator = false)
   	public String notification_upd(){
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notification = notificationService.findById(notificationForm.notification_code);
		notificationDto.setNotification_code(String.valueOf(notificationForm.notification_code));
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg(notificationForm.published_flg);
   	return "notification_upd.jsp";
    }
	
	@Execute(validator = true,input="notification_upd.jsp")
	public String notification_upd_fin() {
		notificationDto.setNotification_code(notificationForm.notification_code);
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg(notificationForm.published_flg);
		notificationService.update(notificationDto);
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
	return "notification_admin.jsp";
    }
	
	/* お知らせ削除 */
	@Execute(validator = true,input="notification_admin.jsp")
   	public String notification_del(){
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
    	notificationDto.setNotification_code(notificationForm.notification_code);
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg(notificationForm.published_flg);
   	return "notification_del.jsp";
    }
	
	@Execute(validator = true,input="notification_upd.jsp")
	public String notification_del_fin() {
		notificationDto.setNotification_code(notificationForm.notification_code);
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg(notificationForm.published_flg);
		notificationService.delete(notificationDto);
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
	return "notification_admin.jsp";
    }
	
	/* お知らせ詳細*/
	@Execute(validator = false)
   	public String notification_detail(){
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
		notificationDto.setNotification_code(notificationForm.notification_code);
		notificationDto.setNotification_genre_code(notificationForm.notification_genre_code);
		notificationDto.setNotification_title(notificationForm.notification_title);
		notificationDto.setNotification_content(notificationForm.notification_content);
		notificationDto.setPublished_day(sdf1.format(date));
		notificationDto.setPublished_flg(notificationForm.published_flg);
   	return "notification_detail.jsp";
    }
}
