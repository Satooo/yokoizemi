package syain.action;

import java.util.List;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.entity.Circle;
import syain.form.StudentForm;
import syain.service.CircleService;


public class StudentAction extends NotificationAction{
	
	public List<Circle>circleList;
	
	@Resource 
	protected CircleService circleService;
	
	@Resource
	@ActionForm
	StudentForm studentForm;
	
	/* 学生総合トップ */
    @Execute(validator = false)
    	public String student() {
    	return "student.jsp";
    }
    
	
	/* サークル */
   /* @Execute(validator = false)
    	public String circle() {
    	return "/student/circle.jsp";
    }
   */
    /* サークル管理者用 */
    @Execute(validator = false)
	public String circle() {
	circleList = circleService.getCircleList();
	return "/student/circle_admin.jsp";
}

    /* サークル一覧表示 */
    @Execute(validator = false)
  	public String srch() {
  	circleList=circleService.getCircleList();
  	return "/student/circleList.jsp";
  }
    
    /*サークル活動内容*/
    @Execute(validator = false)
  	public String   circle_detail () {
 
  	return "/student/circle_detail.jsp";
  }
    
    
    /* 広報 */
    @Execute(validator = false)
    	public String publisher() {
    	studentForm.pass = "/images/a/HCSnews1.pdf";
    	studentForm.pass1 = "/images/a/HCSnews(A4)-1.pdf";
    	studentForm.pass2 = "/images/a/HCSnews(A4)-2.pdf";
    	studentForm.pass3 = "/images/a/HCSnews(A4)-3.pdf";
    	studentForm.pass4 = "/images/a/HCSnews(A4)-4.pdf";
    	return "/student/publisher.jsp";
    }
    

    
    /* 学食 */
    @Execute(validator = false)
    	public String lunch() {
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
    	return "/student/lunch.jsp";
    }
    
    /* 要望 */
    @Execute(validator = false)
    	public String demand() {
    	return "/student/demand.jsp";
    }

}
