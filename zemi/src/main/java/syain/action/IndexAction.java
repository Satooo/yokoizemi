/*
 * Copyright 2004-2008 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package syain.action;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.dto.UserDataDto;
import syain.form.LoginForm;
import syain.service.UserService;


public class IndexAction extends NotificationAction{
	
	@Resource
	@ActionForm
	public LoginForm loginForm;

	@Resource
	protected UserDataDto userDataDto;

	@Resource
	protected UserService userService;

	boolean comfirm = false;

	
	@Execute(validator = true, input = "login.jsp")
	public String index() throws SQLException {
		notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
		notificationSideList = notificationService.findAllOrderById(notificationForm.published_flg);
		comfirm = userService.findByIdPwd(loginForm.userid, loginForm.password);
		if (comfirm == false) {
			return "login.jsp";
		}
		userDataDto.setUserId(loginForm.userid);
		return "index.jsp";
	}
	
	
	
	@Execute(validator = false)
	public String menu3() throws SQLException {
		notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
		notificationSideList = notificationService.findAllOrderById(notificationForm.published_flg);
		return "index.jsp";
	}
}
