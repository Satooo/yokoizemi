package syain.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import syain.dto.DemandDto;
import syain.dto.ScheduleDto;
import syain.dto.SyllabusDto;
import syain.dto.UserDto;
import syain.entity.Demand;
import syain.entity.DemandGenre;
import syain.entity.Schedule;
import syain.entity.Syllabus;
import syain.entity.User;
import syain.form.KyoumuForm;
import syain.service.DemandService;
import syain.service.DepartmentService;
import syain.service.ScheduleService;
import syain.service.SyllabusService;

public class KyoumuAction extends IndexAction {
	/* 教務トップ */
	@Execute(validator = false)
	public String index() {
		return "kyoumu.jsp";
	}

	@Resource
	protected HttpServletRequest request;
	@Resource
	@ActionForm
	protected KyoumuForm kyoumuform;

	@Resource
	public DemandService demandService;

	@Resource
	public DepartmentService departmentService;

	@Resource
	public SyllabusService syllabusService;

	@Resource
	public ScheduleService scheduleService;


	public List<UserDto> userDtoList = new ArrayList<UserDto>();

	public List<User> userList;

	/* 教員 */
	@Execute(validator = false)
	public String teacherInfo() {
		// 学科コードを受け取る

		try {

			// jspから受け取った学科から、学科コードをdepartmentテーブルから検索
			kyoumuform.department_code = departmentService.departmentSearch(kyoumuform.teacherselectcourse);

			// department_code科の教員をuserテーブルから検索
			userList = userService.teacherSearch(kyoumuform.department_code);
			// userEntityの内容をuserDTOへ詰め替え

			// 担当サークルを検索

			for (User ue : userList) {
				UserDto ud = new UserDto();
				// DTO生成
				
				ud.setUserid(ue.userid);
				ud.setUserName(ue.userName);
				ud.setClassCode(ue.classCode);
				ud.setPassword(ue.password);
				ud.setIdFlg(ue.idFlg);
				String circle = userService.circleGet(ue.userid);
				ud.setCircle(circle);
				// DTOリストに追加
				userDtoList.add(ud);
			}
		} catch (NullPointerException e) {
			return "teacherInfo.jsp";
		}

		return "teacherInfo.jsp";
	}

	public List<SyllabusDto> syllabusDtoList = new ArrayList<SyllabusDto>();

	public List<Syllabus> syllabusList;

	/* シラバス */
	@Execute(validator = false)
	public String syllabusInfo() {
		try {
			// jspから学科を受け取る、学科コードをdepartmentテーブルから検索
			kyoumuform.department_code = departmentService.departmentSearch(kyoumuform.syllabusselectcourse);
			// 受け取った学科から、1年から4年のシラバスを検索
			syllabusList = syllabusService.syllabusSearch(kyoumuform.department_code);

			/* シラバス */
			// 学科を入力、1年から4年のシラバスを表示
			// syllabusform.syllabusselectcourse;
			// userEntityの内容をuserDTOへ詰め替え

			for (Syllabus se : syllabusList) {
				SyllabusDto sd = new SyllabusDto();
				// DTO生成
				sd.setSyllabusCode(se.syllabusCode);
				// sd.setUserName(se.userName);
				// sd.setClassCode(se.classCode);
				// sd.setPassword(se.password);
				sd.setSyllabusFile(se.syllabusFile);
				// DTOリストに追加
				syllabusDtoList.add(sd);
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
		}

		return "syllabusInfo.jsp";
	}

	public List<ScheduleDto> scheduleDtoList = new ArrayList<ScheduleDto>();

	public List<Schedule> scheduleList;

	// 講義予定表
	@Execute(validator = false)
	public String schedule() {
		try {
			// jspから学科を受け取る、学科コードをdepartmentテーブルから検索
			kyoumuform.department_code = departmentService.departmentSearch(kyoumuform.scheduleselectcourse);

			// 受け取った学科、学年から、先月、今月、来月の講義予定を検索
			// Listで受け取る
			scheduleList = scheduleService.scheduleSearch(kyoumuform.department_code,
					kyoumuform.scheduleselectschoolyear);

			// ScheduleEntityの内容をScheduleDtoへ詰め替え
			for (Schedule se : scheduleList) {
				ScheduleDto sd = new ScheduleDto();
				// DTO生成
				// sd.setUserid(se.userid);
				// sd.setUserName(se.userName);
				// sd.setClassCode(se.classCode);
				// sd.setPassword(se.password);
				sd.setScheduleFile(se.scheduleFile);
				// DTOリストに追加
				scheduleDtoList.add(sd);
			}
		} catch (NullPointerException e) {
			return "schedule.jsp";
		}

		return "schedule.jsp";
	}

	public List<Demand> demandList;

	@Resource
	protected DemandDto demanddto;

	// 要望フォーム
	@Execute(validator = false)
	public String demand() {
		return "demand/demand.jsp";
	}

	// 要望フォーム確認画面
	@Execute(validator = false)
	public String kakunin() {
		return "demand/kakunin.jsp";
	}

	public DemandGenre demandgenre;

	// 要望フォーム完了画面
	@Execute(validator = false, input = "kakunin.jsp")
	public String kanryo() {

		// もっとも大きなコード+1を取得
		demanddto.setDemandCode(demandService.maxCode());
		// ジャンルからジャンルコードを検索
		demanddto.setDemandGenreCode(demandService.genreSearch(kyoumuform.demand_genre));
		demanddto.setDemandContent(kyoumuform.demand_content);
		// 自分のidをset
		demanddto.setStudentId(userDataDto.getUserId());
		// 現在の日付をセット
		Date date = new Date();
		System.out.println(date.toString());
		demanddto.setDemandDate(date);
		try {
			if (demandService.insert(demanddto) > 0) {
				kyoumuform.message = "送信しました";
				return "demand/kanryo.jsp";
			} else {
				kyoumuform.message = "送信に失敗しました";
				return "demand/kanryo.jsp";
			}
		} catch (Exception e) {
			kyoumuform.message = "送信に失敗しました";
			return "demand/kanryo.jsp";
		}

	}

	@Resource
	protected HttpServletResponse response;

	@Execute(validator = false)
	public String download() {
		response.setContentType("application/octet-stream");
		response.setHeader("Content-disposition", "attachment; filename=\"" + kyoumuform.demand_genre + "\"");
		return null;
	}

}
