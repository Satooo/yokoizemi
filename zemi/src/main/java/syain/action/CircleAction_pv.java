package syain.action;

import org.seasar.struts.annotation.Execute;

public class CircleAction_pv {
	
    /* サークルリスト */
    @Execute(validator = false)
    	public String circleList() {
    	return "/circle/circleList.jsp";
    }

}
