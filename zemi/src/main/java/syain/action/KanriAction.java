package syain.action;

import java.util.List;
import javax.annotation.Resource;

import org.seasar.struts.annotation.Execute;

import syain.entity.Part;
import syain.service.PartService;

public class KanriAction extends NotificationAction{
	
	public List<Part> partList;
	
	@Resource
	protected PartService partService;
	
	
	/* 管理トップ */
    @Execute(validator = false)
    	public String kanri() {
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
    	return "kanri.jsp";
    }
    
    /* お知らせ */
    @Execute(validator = false)
    	public String notification_admin() {
    	notificationSideList= notificationService.findAllOrderById(notificationForm.published_flg);
		notificationList = notificationService.findAllOrderByIdAdmin();
    	return "/notification/notification_admin.jsp";
    }
    
    /* バイト */
    @Execute(validator = false)
    	public String baito() {
    	partList = partService.findAllOrderById();
    	return "/part/part_admin.jsp";
    }
    
    /* 掲示板 */
    @Execute(validator = false)
    	public String keijiban() {
    	return "keijiban.jsp";
    }
    
    /* 要望 */
    @Execute(validator = false)
    	public String yobo() {
    	return "yobo.jsp";
    }
    
    /*講義予定 */
    @Execute(validator = false)
    	public String kogiyotei() {
    	return "kogiyotei.jsp";
    }
    
    /* 広報 */
    @Execute(validator = false)
    	public String koho() {
    	return "koho.jsp";
    }
}
