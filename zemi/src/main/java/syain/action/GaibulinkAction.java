package syain.action;

import java.sql.SQLException;

import org.seasar.struts.annotation.Execute;

public class GaibulinkAction extends NotificationAction{
	
	/* 外部リンク */
    @Execute(validator = false)
    	public String link() {
		notificationSideList = notificationService.findAllOrderById(notificationForm.published_flg);
    	return "/gaibulink/link.jsp";
    }
    
    /* 戻るボタン */
	@Execute(validator = false)
	public String menu3() throws SQLException {
		notificationList = notificationService.findAllOrderById(notificationForm.published_flg);
		notificationSideList = notificationService.findAllOrderById(notificationForm.published_flg);
		return "/";
	} 
}
